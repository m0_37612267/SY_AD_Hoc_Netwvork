#ifndef UART_H
#define UART_H

#include <QtSerialPort/QSerialPort>         // 提供访问串口的功能
#include <QtSerialPort/QSerialPortInfo>     // 提供系统中存在的串口信息


class BspSerialControl
{
public:
    BspSerialControl(void);
    ~BspSerialControl(void);
    QSerialPort* serial; //串口对象
    QStringList search_serial_port(void); //搜寻com口
    void serial_open(QString strCom, QString StrBaud); //打开串口
    void serial_close(void); //关闭串口
    QSerialPort::SerialPortError get_serial_err(void);
    void serial_write(QByteArray strData); //写串口
    QByteArray serial_read(void); //读串口
private:

};

#endif // UART_H
