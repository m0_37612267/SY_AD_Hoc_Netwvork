QT       += core gui
QT       += serialport
QT       += axcontainer
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
greaterThan(QT_MAJOR_VERSION, 5): QT += core5compat

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    at_protocol.cpp \
    bsp_file.cpp \
    bsp_timer.cpp \
    bsp_uart.cpp \
    flat_ui.cpp \
    form_linkage.cpp \
    main.cpp \
    mainwindow.cpp \
    monitor_ctrl.cpp \
    networking_control.cpp \
    system_config.cpp \
    uart_trsprt.cpp \
    update_ctrl.cpp

HEADERS += \
    at_protocol.h \
    bsp_file.h \
    bsp_timer.h \
    bsp_uart.h \
    flat_ui.h \
    form_linkage.h \
    mainwindow.h \
    monitor_ctrl.h \
    networking_control.h \
    system_config.h \
    uart_trsprt.h \
    update_ctrl.h

FORMS += \
    mainwindow.ui \
    system_config.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
