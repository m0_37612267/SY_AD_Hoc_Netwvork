/********************************************************************************
** Form generated from reading UI file 'local_config_form.ui'
**
** Created by: Qt User Interface Compiler version 6.5.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOCAL_CONFIG_FORM_H
#define UI_LOCAL_CONFIG_FORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Local_Config_Form
{
public:

    void setupUi(QWidget *Local_Config_Form)
    {
        if (Local_Config_Form->objectName().isEmpty())
            Local_Config_Form->setObjectName("Local_Config_Form");
        Local_Config_Form->resize(640, 480);

        retranslateUi(Local_Config_Form);

        QMetaObject::connectSlotsByName(Local_Config_Form);
    } // setupUi

    void retranslateUi(QWidget *Local_Config_Form)
    {
        Local_Config_Form->setWindowTitle(QCoreApplication::translate("Local_Config_Form", "Form", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Local_Config_Form: public Ui_Local_Config_Form {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOCAL_CONFIG_FORM_H
