#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "flat_ui.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    /*风格美化*/
    this->setStyleSheet("*{outline:0px;}QWidget#frmFlatUI{background:#FFFFFF;}");
    // FlatUI::setComboBoxQss(ui->comboBox_uartBaudRate);
    // FlatUI::setComboBoxQss(ui->comboBox_uartPort);
    // FlatUI::setPushButtonQss(ui->pushButton_uartRefresh);
    // FlatUI::setPushButtonQss(ui->pushButton_uartOpen);
    // FlatUI::setPushButtonQss(ui->pushButton_manual_refresh);
    // FlatUI::setLineEditQss(ui->lineEdit_auto_refresh);


    /*槽关联*/
    /*按键关联*/
    connect(ui->pushButton_uartRefresh, SIGNAL(clicked()), this, SLOT(push_button_clicked_uart_refresh())); //刷新串口
    connect(ui->pushButton_uartOpen, SIGNAL(clicked()), this, SLOT(push_button_clicked_uart_open())); //打开串口
    connect(ui->pushButton_networking_config, SIGNAL(clicked()), this, SLOT(action_open_analyser_tool_proccess())); //配置组网
    connect(ui->pushButton_manual_refresh, SIGNAL(clicked()), this, SLOT(push_button_clicked_manual_refresh())); //手动刷新
    connect(ui->checkBox_auto_refresh, SIGNAL(clicked()), this, SLOT(check_box_clicked_auto_refresh())); //手动刷新

    /*串口接收关联*/
    connect(bspSerialCtrl.serial, SIGNAL(readyRead()), this, SLOT(uart_read_data())); //串口接收

    /*定时器关联*/
    connect(bspTimerCtrl.serial_recv_timer, SIGNAL(timeout()), this, SLOT(uart_recv_timeout())); //串口接收完成
    connect(bspTimerCtrl.serial_open_check_timer, SIGNAL(timeout()), this, SLOT(uart_open_check_timeout())); //串口有效性检查
    connect(bspTimerCtrl.auto_refresh_timer, SIGNAL(timeout()), this, SLOT(auto_refresh_timeout())); //自动刷新设备列表

    /*菜单关联*/
    connect(ui->action_system_config, SIGNAL(triggered()), this, SLOT(action_open_analyser_tool_proccess()));

    /*子窗口关联*/
    connect(sys_config_widget, SIGNAL(uart_send_to_main_window_signal(QByteArray)), this, SLOT(uart_recv_from_widget(QByteArray))); //子窗口->主窗口的串口数据传递
    connect(this, SIGNAL(uart_send_to_widget_signal(QByteArray)), sys_config_widget, SLOT(uart_recv_from_main_window(QByteArray))); //主窗口->子窗口的串口数据传递
    connect(sys_config_widget, SIGNAL(table_item_sync_to_main_window_signal()), this, SLOT(table_item_recv_from_widget())); //子窗口->主窗口的列表信息同步
    connect(this, SIGNAL(manual_refresh_list_signal()), sys_config_widget, SLOT(manual_refresh_list_from_main_window())); //主窗口->子窗口的手动刷新指令传递

    /*串口初始化*/
    serial_init();
    /*主窗口列表与定时器初始化*/
    main_window_init();

    /*设置主窗口运行日志的存储地址*/
    bspFileCtrl.set_run_log_path("./mainwindow_log.txt");
    bspFileCtrl.write_run_log(QtInfoMsg, bspTimerCtrl.get_current_timestamp(),"start run tool");

}

MainWindow::~MainWindow()
{
    delete ui;
}
