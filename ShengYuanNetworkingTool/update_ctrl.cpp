#include "update_ctrl.h"
#include "system_config.h"
#include "ui_system_config.h"
#include <QMessageBox>

UpdateCtrl::UpdateCtrl() {}

/*选择升级文件*/
void SystemConfig::push_button_clicked_select_update_file(void)
{
    /*选择文件*/
    updateCtrl.updateFilePath = bspFileCtrl.select_hex_file(this, ui->lineEdit_updateFilePath->text());
    /*路径回显*/
    ui->lineEdit_updateFilePath->setText(updateCtrl.updateFilePath);

    bspFileCtrl.save_setting(E_SETTING_UPDATE_PATH, updateCtrl.updateFilePath);
}

void SystemConfig::push_button_clicked_update(void)
{
    if("开始升级" == ui->pushButton_update->text())
    {
        ui->pushButton_update->setText("停止升级");
        updateCtrl.eState = E_UPGRADE_START;
        upgrade_state_machine();
    }
    else
    {
        qDebug() << bspTimerCtrl.get_current_timestamp();
        ui->pushButton_update->setText("开始升级");
        updateCtrl.eState = E_FINISH;
    }
}

void SystemConfig::upgrade_state_machine(void)
{
    static QFile file;
    static QByteArray sn;
    // static QByteArray routesn;
    // static uint8_t temp_param_en;
    // static uint8_t temp_ch;
    // static uint8_t temp_enc;
    uint8_t result;
    QChar ch;

    switch(updateCtrl.eState)
    {
    case E_UPGRADE_IDLE:
        break;
    case E_FINISH:
        /*关闭文件*/
        file.close();
        updateCtrl.eState = E_UPGRADE_IDLE;
        if(1 == updateCtrl.Loop_Update_Flg)
        {
            updateCtrl.eState = E_UPGRADE_START;
        }
        break;
    case E_UPGRADE_START:
        /*设置路径，打开文件*/
        qDebug() << "update start";
        if(1 == updateCtrl.Update_Print_Flg)
        {
            ui->textEdit_analyseOutput->append("update start");
            /*存储日志*/
            bspFileCtrl.write_run_log(QtInfoMsg, bspTimerCtrl.get_current_timestamp(),"update start");
        }
        file.setFileName(updateCtrl.updateFilePath);
        if (file.open(QIODevice::ReadOnly))
        {
            qDebug() << "file open";
            if(1 == updateCtrl.Update_Print_Flg)
            {
                ui->textEdit_analyseOutput->append("file open");
                /*存储日志*/
                bspFileCtrl.write_run_log(QtInfoMsg, bspTimerCtrl.get_current_timestamp(),"file open");
            }
            ui->progressBar_update->reset();
            updateCtrl.fullPackSize = file.size();
            updateCtrl.currentSize = 0;
            updateCtrl.eState = E_UPGRADE_READY;
            if(E_CONFIG_REMOTE_MODE == monitor_ctrl.eConfigMode)
            {
                // if((8 < ui->lineEdit_destId->text().size()))
                // {
                //     QMessageBox::critical(this, "错误信息", "请填写符合格式的目标地址信息");
                //     updateCtrl.eState = E_FINISH;
                //     break;
                // }
                sn.clear();
                sn = ui->lineEdit_destId->text().toUtf8();
                qDebug() << sn;
            }
            else if(E_CONFIG_BROADCAST_MODE == monitor_ctrl.eConfigMode)
            {
                QMessageBox::critical(this, "错误信息", "暂不支持广播升级");
                updateCtrl.eState = E_FINISH;
                break;
            }
        }
        break;
    case E_UPGRADE_READY:
        /*逐行读取hex文件，协议组包*/
        if(!file.atEnd())
        {
            QByteArray line = file.readLine();
            updateCtrl.currentSize += line.size();
            //qDebug() << line;
            atPrtcl.assemble_cmd = E_AT_UPDATE;
            atPrtcl.assemble_buff.resize(0);
            atPrtcl.analyse_cmd = atPrtcl.assemble_cmd;
            atPrtcl.analyse_id.resize(0);
            if(E_CONFIG_LOACL_MODE == monitor_ctrl.eConfigMode)
            {
                atPrtcl.assemble_buff.append("FFFFFFFF");
            }
            else if(E_CONFIG_BROADCAST_MODE == monitor_ctrl.eConfigMode)
            {
                atPrtcl.assemble_buff.append("AAAAAAAA");
            }
            else
            {
                atPrtcl.assemble_buff.append(ui->lineEdit_destId->text().toUtf8());
            }
            atPrtcl.assemble_buff.append(',');
            if(0 == ui->comboBox_updateTarget->currentIndex())
            {
                atPrtcl.assemble_buff.append('0');
            }
            else
            {
                atPrtcl.assemble_buff.append('1');
            }
            atPrtcl.assemble_buff.append(',');
            line = line.mid(1,(line.size()-3));
            updateCtrl.latchAddr = line.mid(2,2);
            atPrtcl.assemble_buff.append(line);

            atPrtcl.analyse_id.append(atPrtcl.assemble_buff);

            atPrtcl.at_protocol_assemble(atPrtcl.assemble_cmd, &atPrtcl.assemble_buff, &atPrtcl.assemble_buff);
            //updateCtrl.sendPack = updateCtrl.update_pack_assemble(E_LOCAL_UPDATE,dwAddr,0,0,0,0,line,&updateCtrl.latchAddrH,&updateCtrl.latchAddrL);

            qDebug() << atPrtcl.assemble_buff.toHex();
            if(1 == updateCtrl.Update_Print_Flg)
            {
                ui->textEdit_rawOutput->append(atPrtcl.assemble_buff.toHex());
                /*存储日志*/
                bspFileCtrl.write_run_log(QtInfoMsg, bspTimerCtrl.get_current_timestamp(),atPrtcl.assemble_buff.toHex());
            }
            updateCtrl.eState = E_SEND_PACK;
        }
        else
        {
            bspTimerCtrl.stop_timer(bspTimerCtrl.update_ack_timer);
            updateCtrl.eState = E_FINISH;
        }
        break;
    case E_RESEND_PACK:
        ui->textEdit_rawOutput->append("*");
    case E_SEND_PACK:
        /*串口发包*/
        uart_send_to_main_window(atPrtcl.assemble_buff);
        updateCtrl.eState = E_WAIT_ACK;
        updateCtrl.updateRecvFlg = 0;
        qDebug() << "send over";
        // if(1 == updateCtrl.Update_Print_Flg)
        // {
        //     ui->textEdit_uart->append("send over");
        //     /*存储日志*/
        //     bspFileCtrl.write_run_log(QtInfoMsg, bspTimerCtrl.get_current_timestamp(),"send over");
        // }
        if(E_CONFIG_LOACL_MODE == monitor_ctrl.eConfigMode)
        {
            bspTimerCtrl.start_timer(bspTimerCtrl.update_ack_timer,50);
        }
        else if(E_CONFIG_REMOTE_MODE == monitor_ctrl.eConfigMode)
        {
            /*直连升级，150ms*/
            bspTimerCtrl.start_timer(bspTimerCtrl.update_ack_timer,150);
        }
        break;
    case E_WAIT_ACK:
        /*等待应答*/
        if(1 == updateCtrl.updateRecvFlg)
        {
            qDebug() << "recv pack" << updateCtrl.recvPack;
            // if(1 == updateCtrl.Update_Print_Flg)
            // {
            //     ui->textEdit_uart->append("recv pack " + updateCtrl.recvPack.toHex());
            //     /*存储日志*/
            //     bspFileCtrl.write_run_log(QtInfoMsg, bspTimerCtrl.get_current_timestamp(),"recv pack " + updateCtrl.recvPack.toHex());
            // }
            updateCtrl.updateRecvFlg = 0;
            /*协议解析*/
            result = AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 3).toInt();
            if(E_CONFIG_REMOTE_MODE == monitor_ctrl.eConfigMode)
            {
                if(AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 1) != ui->lineEdit_destId->text().toUtf8())
                {
                    /*ID不匹配*/
                    result = 0xFF;
                }
            }
            if(AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 2) != updateCtrl.latchAddr)
            {
                /*升级地址不匹配*/
                result = 0xFF;
            }

            if(0 == result)
            {
                /*解析成功*/
                qDebug() << bspTimerCtrl.get_current_timestamp();
                qDebug() << "ack success";
                if(1 == updateCtrl.Update_Print_Flg)
                {
                    ui->textEdit_analyseOutput->append(bspTimerCtrl.get_current_timestamp());
                    ui->textEdit_analyseOutput->append(QString("ack success"));
                    /*存储日志*/
                    bspFileCtrl.write_run_log(QtInfoMsg, bspTimerCtrl.get_current_timestamp(),"ack success");
                }
                if(updateCtrl.updatePercent != updateCtrl.currentSize*100/updateCtrl.fullPackSize)
                {
                    updateCtrl.updatePercent = updateCtrl.currentSize*100/updateCtrl.fullPackSize;
                    ui->progressBar_update->setValue(updateCtrl.updatePercent);
                }
                bspTimerCtrl.stop_timer(bspTimerCtrl.update_ack_timer);
                updateCtrl.eState = E_UPGRADE_READY;
            }
            else if((2 == result)
                     ||((file.atEnd()) /*&& (255 == result)*/))
            {
                /*升级完成&校验成功*/
                qDebug() << bspTimerCtrl.get_current_timestamp();
                qDebug() << "update success";
                if(1 == updateCtrl.Update_Print_Flg)
                {
                    ui->textEdit_analyseOutput->append(bspTimerCtrl.get_current_timestamp());
                    ui->textEdit_analyseOutput->append("update success\n");
                    /*存储日志*/
                    bspFileCtrl.write_run_log(QtInfoMsg, bspTimerCtrl.get_current_timestamp(),"update success");
                }
                bspTimerCtrl.stop_timer(bspTimerCtrl.update_ack_timer);
                ui->progressBar_update->setValue(100);
                updateCtrl.eState = E_FINISH;
            }
            else
            {
                /*重发*/
                ui->textEdit_analyseOutput->append(QString("err: %1").arg(result));
                updateCtrl.eState = E_RESEND_PACK;
            }
        }
        //TODO增加重发计时器
        break;
    default:
        return;
    }

    if(E_UPGRADE_IDLE != updateCtrl.eState)
    {
        /*如果没有升级完成，则利用信号槽机制重复调用升级状态机*/
        bspTimerCtrl.start_timer(bspTimerCtrl.update_state_timer,1);
    }
}

void SystemConfig::ack_timeout_proccess(void)
{
    qDebug() << "timeout";
    if(1 == updateCtrl.Update_Print_Flg)
    {
        ui->textEdit_analyseOutput->append(QString("timeout"));
        /*存储日志*/
        bspFileCtrl.write_run_log(QtInfoMsg, bspTimerCtrl.get_current_timestamp(),"timeout");
    }
    bspTimerCtrl.stop_timer(bspTimerCtrl.update_ack_timer);
    if((E_FINISH != updateCtrl.eState) && (E_UPGRADE_IDLE != updateCtrl.eState))
    {
        updateCtrl.eState = E_RESEND_PACK;
    }
}

void SystemConfig::update_state_timeout_proccess(void)
{
    bspTimerCtrl.stop_timer(bspTimerCtrl.update_state_timer);
    upgrade_state_machine();
}
