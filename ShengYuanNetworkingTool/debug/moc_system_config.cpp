/****************************************************************************
** Meta object code from reading C++ file 'system_config.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.5.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../system_config.h"
#include <QtCore/qmetatype.h>

#if __has_include(<QtCore/qtmochelpers.h>)
#include <QtCore/qtmochelpers.h>
#else
QT_BEGIN_MOC_NAMESPACE
#endif


#include <memory>

#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'system_config.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.5.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

#ifndef Q_CONSTINIT
#define Q_CONSTINIT
#endif

QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
QT_WARNING_DISABLE_GCC("-Wuseless-cast")
namespace {

#ifdef QT_MOC_HAS_STRINGDATA
struct qt_meta_stringdata_CLASSSystemConfigENDCLASS_t {};
static constexpr auto qt_meta_stringdata_CLASSSystemConfigENDCLASS = QtMocHelpers::stringData(
    "SystemConfig",
    "uart_send_to_main_window_signal",
    "",
    "data",
    "table_item_sync_to_main_window_signal",
    "combox_changed_config_mode_change",
    "index",
    "push_button_clicked_scan_device",
    "push_button_clicked_check_device_list",
    "push_button_clicked_add_device",
    "push_button_clicked_delete_device",
    "push_button_clicked_at_test",
    "push_button_clicked_get_version",
    "push_button_clicked_reset",
    "push_button_clicked_format",
    "push_button_clicked_get_device_state",
    "push_button_clicked_clear_device_state",
    "push_button_clicked_transfer",
    "push_button_hide_output",
    "push_button_clicked_select_update_file",
    "push_button_clicked_update",
    "uart_recv_from_main_window",
    "uart_send_to_main_window",
    "manual_refresh_list_from_main_window",
    "ack_timeout_proccess",
    "update_state_timeout_proccess"
);
#else  // !QT_MOC_HAS_STRING_DATA
struct qt_meta_stringdata_CLASSSystemConfigENDCLASS_t {
    uint offsetsAndSizes[52];
    char stringdata0[13];
    char stringdata1[32];
    char stringdata2[1];
    char stringdata3[5];
    char stringdata4[38];
    char stringdata5[34];
    char stringdata6[6];
    char stringdata7[32];
    char stringdata8[38];
    char stringdata9[31];
    char stringdata10[34];
    char stringdata11[28];
    char stringdata12[32];
    char stringdata13[26];
    char stringdata14[27];
    char stringdata15[37];
    char stringdata16[39];
    char stringdata17[29];
    char stringdata18[24];
    char stringdata19[39];
    char stringdata20[27];
    char stringdata21[27];
    char stringdata22[25];
    char stringdata23[37];
    char stringdata24[21];
    char stringdata25[30];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(sizeof(qt_meta_stringdata_CLASSSystemConfigENDCLASS_t::offsetsAndSizes) + ofs), len 
Q_CONSTINIT static const qt_meta_stringdata_CLASSSystemConfigENDCLASS_t qt_meta_stringdata_CLASSSystemConfigENDCLASS = {
    {
        QT_MOC_LITERAL(0, 12),  // "SystemConfig"
        QT_MOC_LITERAL(13, 31),  // "uart_send_to_main_window_signal"
        QT_MOC_LITERAL(45, 0),  // ""
        QT_MOC_LITERAL(46, 4),  // "data"
        QT_MOC_LITERAL(51, 37),  // "table_item_sync_to_main_windo..."
        QT_MOC_LITERAL(89, 33),  // "combox_changed_config_mode_ch..."
        QT_MOC_LITERAL(123, 5),  // "index"
        QT_MOC_LITERAL(129, 31),  // "push_button_clicked_scan_device"
        QT_MOC_LITERAL(161, 37),  // "push_button_clicked_check_dev..."
        QT_MOC_LITERAL(199, 30),  // "push_button_clicked_add_device"
        QT_MOC_LITERAL(230, 33),  // "push_button_clicked_delete_de..."
        QT_MOC_LITERAL(264, 27),  // "push_button_clicked_at_test"
        QT_MOC_LITERAL(292, 31),  // "push_button_clicked_get_version"
        QT_MOC_LITERAL(324, 25),  // "push_button_clicked_reset"
        QT_MOC_LITERAL(350, 26),  // "push_button_clicked_format"
        QT_MOC_LITERAL(377, 36),  // "push_button_clicked_get_devic..."
        QT_MOC_LITERAL(414, 38),  // "push_button_clicked_clear_dev..."
        QT_MOC_LITERAL(453, 28),  // "push_button_clicked_transfer"
        QT_MOC_LITERAL(482, 23),  // "push_button_hide_output"
        QT_MOC_LITERAL(506, 38),  // "push_button_clicked_select_up..."
        QT_MOC_LITERAL(545, 26),  // "push_button_clicked_update"
        QT_MOC_LITERAL(572, 26),  // "uart_recv_from_main_window"
        QT_MOC_LITERAL(599, 24),  // "uart_send_to_main_window"
        QT_MOC_LITERAL(624, 36),  // "manual_refresh_list_from_main..."
        QT_MOC_LITERAL(661, 20),  // "ack_timeout_proccess"
        QT_MOC_LITERAL(682, 29)   // "update_state_timeout_proccess"
    },
    "SystemConfig",
    "uart_send_to_main_window_signal",
    "",
    "data",
    "table_item_sync_to_main_window_signal",
    "combox_changed_config_mode_change",
    "index",
    "push_button_clicked_scan_device",
    "push_button_clicked_check_device_list",
    "push_button_clicked_add_device",
    "push_button_clicked_delete_device",
    "push_button_clicked_at_test",
    "push_button_clicked_get_version",
    "push_button_clicked_reset",
    "push_button_clicked_format",
    "push_button_clicked_get_device_state",
    "push_button_clicked_clear_device_state",
    "push_button_clicked_transfer",
    "push_button_hide_output",
    "push_button_clicked_select_update_file",
    "push_button_clicked_update",
    "uart_recv_from_main_window",
    "uart_send_to_main_window",
    "manual_refresh_list_from_main_window",
    "ack_timeout_proccess",
    "update_state_timeout_proccess"
};
#undef QT_MOC_LITERAL
#endif // !QT_MOC_HAS_STRING_DATA
} // unnamed namespace

Q_CONSTINIT static const uint qt_meta_data_CLASSSystemConfigENDCLASS[] = {

 // content:
      11,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags, initial metatype offsets
       1,    1,  146,    2, 0x06,    1 /* Public */,
       4,    0,  149,    2, 0x06,    3 /* Public */,

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
       5,    1,  150,    2, 0x08,    4 /* Private */,
       7,    0,  153,    2, 0x08,    6 /* Private */,
       8,    0,  154,    2, 0x08,    7 /* Private */,
       9,    0,  155,    2, 0x08,    8 /* Private */,
      10,    0,  156,    2, 0x08,    9 /* Private */,
      11,    0,  157,    2, 0x08,   10 /* Private */,
      12,    0,  158,    2, 0x08,   11 /* Private */,
      13,    0,  159,    2, 0x08,   12 /* Private */,
      14,    0,  160,    2, 0x08,   13 /* Private */,
      15,    0,  161,    2, 0x08,   14 /* Private */,
      16,    0,  162,    2, 0x08,   15 /* Private */,
      17,    0,  163,    2, 0x08,   16 /* Private */,
      18,    0,  164,    2, 0x08,   17 /* Private */,
      19,    0,  165,    2, 0x08,   18 /* Private */,
      20,    0,  166,    2, 0x08,   19 /* Private */,
      21,    1,  167,    2, 0x08,   20 /* Private */,
      22,    1,  170,    2, 0x08,   22 /* Private */,
      23,    0,  173,    2, 0x08,   24 /* Private */,
      24,    0,  174,    2, 0x08,   25 /* Private */,
      25,    0,  175,    2, 0x08,   26 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QByteArray,    3,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,    3,
    QMetaType::Void, QMetaType::QByteArray,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

Q_CONSTINIT const QMetaObject SystemConfig::staticMetaObject = { {
    QMetaObject::SuperData::link<QWidget::staticMetaObject>(),
    qt_meta_stringdata_CLASSSystemConfigENDCLASS.offsetsAndSizes,
    qt_meta_data_CLASSSystemConfigENDCLASS,
    qt_static_metacall,
    nullptr,
    qt_incomplete_metaTypeArray<qt_meta_stringdata_CLASSSystemConfigENDCLASS_t,
        // Q_OBJECT / Q_GADGET
        QtPrivate::TypeAndForceComplete<SystemConfig, std::true_type>,
        // method 'uart_send_to_main_window_signal'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<QByteArray, std::false_type>,
        // method 'table_item_sync_to_main_window_signal'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'combox_changed_config_mode_change'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<int, std::false_type>,
        // method 'push_button_clicked_scan_device'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'push_button_clicked_check_device_list'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'push_button_clicked_add_device'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'push_button_clicked_delete_device'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'push_button_clicked_at_test'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'push_button_clicked_get_version'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'push_button_clicked_reset'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'push_button_clicked_format'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'push_button_clicked_get_device_state'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'push_button_clicked_clear_device_state'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'push_button_clicked_transfer'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'push_button_hide_output'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'push_button_clicked_select_update_file'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'push_button_clicked_update'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'uart_recv_from_main_window'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<QByteArray, std::false_type>,
        // method 'uart_send_to_main_window'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<QByteArray, std::false_type>,
        // method 'manual_refresh_list_from_main_window'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'ack_timeout_proccess'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'update_state_timeout_proccess'
        QtPrivate::TypeAndForceComplete<void, std::false_type>
    >,
    nullptr
} };

void SystemConfig::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<SystemConfig *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->uart_send_to_main_window_signal((*reinterpret_cast< std::add_pointer_t<QByteArray>>(_a[1]))); break;
        case 1: _t->table_item_sync_to_main_window_signal(); break;
        case 2: _t->combox_changed_config_mode_change((*reinterpret_cast< std::add_pointer_t<int>>(_a[1]))); break;
        case 3: _t->push_button_clicked_scan_device(); break;
        case 4: _t->push_button_clicked_check_device_list(); break;
        case 5: _t->push_button_clicked_add_device(); break;
        case 6: _t->push_button_clicked_delete_device(); break;
        case 7: _t->push_button_clicked_at_test(); break;
        case 8: _t->push_button_clicked_get_version(); break;
        case 9: _t->push_button_clicked_reset(); break;
        case 10: _t->push_button_clicked_format(); break;
        case 11: _t->push_button_clicked_get_device_state(); break;
        case 12: _t->push_button_clicked_clear_device_state(); break;
        case 13: _t->push_button_clicked_transfer(); break;
        case 14: _t->push_button_hide_output(); break;
        case 15: _t->push_button_clicked_select_update_file(); break;
        case 16: _t->push_button_clicked_update(); break;
        case 17: _t->uart_recv_from_main_window((*reinterpret_cast< std::add_pointer_t<QByteArray>>(_a[1]))); break;
        case 18: _t->uart_send_to_main_window((*reinterpret_cast< std::add_pointer_t<QByteArray>>(_a[1]))); break;
        case 19: _t->manual_refresh_list_from_main_window(); break;
        case 20: _t->ack_timeout_proccess(); break;
        case 21: _t->update_state_timeout_proccess(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (SystemConfig::*)(QByteArray );
            if (_t _q_method = &SystemConfig::uart_send_to_main_window_signal; *reinterpret_cast<_t *>(_a[1]) == _q_method) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (SystemConfig::*)();
            if (_t _q_method = &SystemConfig::table_item_sync_to_main_window_signal; *reinterpret_cast<_t *>(_a[1]) == _q_method) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject *SystemConfig::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SystemConfig::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CLASSSystemConfigENDCLASS.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int SystemConfig::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 22)
            *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType();
        _id -= 22;
    }
    return _id;
}

// SIGNAL 0
void SystemConfig::uart_send_to_main_window_signal(QByteArray _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void SystemConfig::table_item_sync_to_main_window_signal()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}
QT_WARNING_POP
