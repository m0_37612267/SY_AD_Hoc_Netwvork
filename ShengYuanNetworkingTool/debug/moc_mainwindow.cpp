/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.5.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mainwindow.h"
#include <QtCore/qmetatype.h>

#if __has_include(<QtCore/qtmochelpers.h>)
#include <QtCore/qtmochelpers.h>
#else
QT_BEGIN_MOC_NAMESPACE
#endif


#include <memory>

#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.5.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

#ifndef Q_CONSTINIT
#define Q_CONSTINIT
#endif

QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
QT_WARNING_DISABLE_GCC("-Wuseless-cast")
namespace {

#ifdef QT_MOC_HAS_STRINGDATA
struct qt_meta_stringdata_CLASSMainWindowENDCLASS_t {};
static constexpr auto qt_meta_stringdata_CLASSMainWindowENDCLASS = QtMocHelpers::stringData(
    "MainWindow",
    "uart_send_to_widget_signal",
    "",
    "manual_refresh_list_signal",
    "push_button_clicked_uart_refresh",
    "push_button_clicked_uart_open",
    "push_button_clicked_manual_refresh",
    "check_box_clicked_auto_refresh",
    "uart_read_data",
    "uart_recv_timeout",
    "uart_open_check_timeout",
    "auto_refresh_timeout",
    "action_open_analyser_tool_proccess",
    "uart_recv_from_widget",
    "data",
    "uart_send_to_widget",
    "table_item_recv_from_widget"
);
#else  // !QT_MOC_HAS_STRING_DATA
struct qt_meta_stringdata_CLASSMainWindowENDCLASS_t {
    uint offsetsAndSizes[34];
    char stringdata0[11];
    char stringdata1[27];
    char stringdata2[1];
    char stringdata3[27];
    char stringdata4[33];
    char stringdata5[30];
    char stringdata6[35];
    char stringdata7[31];
    char stringdata8[15];
    char stringdata9[18];
    char stringdata10[24];
    char stringdata11[21];
    char stringdata12[35];
    char stringdata13[22];
    char stringdata14[5];
    char stringdata15[20];
    char stringdata16[28];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(sizeof(qt_meta_stringdata_CLASSMainWindowENDCLASS_t::offsetsAndSizes) + ofs), len 
Q_CONSTINIT static const qt_meta_stringdata_CLASSMainWindowENDCLASS_t qt_meta_stringdata_CLASSMainWindowENDCLASS = {
    {
        QT_MOC_LITERAL(0, 10),  // "MainWindow"
        QT_MOC_LITERAL(11, 26),  // "uart_send_to_widget_signal"
        QT_MOC_LITERAL(38, 0),  // ""
        QT_MOC_LITERAL(39, 26),  // "manual_refresh_list_signal"
        QT_MOC_LITERAL(66, 32),  // "push_button_clicked_uart_refresh"
        QT_MOC_LITERAL(99, 29),  // "push_button_clicked_uart_open"
        QT_MOC_LITERAL(129, 34),  // "push_button_clicked_manual_re..."
        QT_MOC_LITERAL(164, 30),  // "check_box_clicked_auto_refresh"
        QT_MOC_LITERAL(195, 14),  // "uart_read_data"
        QT_MOC_LITERAL(210, 17),  // "uart_recv_timeout"
        QT_MOC_LITERAL(228, 23),  // "uart_open_check_timeout"
        QT_MOC_LITERAL(252, 20),  // "auto_refresh_timeout"
        QT_MOC_LITERAL(273, 34),  // "action_open_analyser_tool_pro..."
        QT_MOC_LITERAL(308, 21),  // "uart_recv_from_widget"
        QT_MOC_LITERAL(330, 4),  // "data"
        QT_MOC_LITERAL(335, 19),  // "uart_send_to_widget"
        QT_MOC_LITERAL(355, 27)   // "table_item_recv_from_widget"
    },
    "MainWindow",
    "uart_send_to_widget_signal",
    "",
    "manual_refresh_list_signal",
    "push_button_clicked_uart_refresh",
    "push_button_clicked_uart_open",
    "push_button_clicked_manual_refresh",
    "check_box_clicked_auto_refresh",
    "uart_read_data",
    "uart_recv_timeout",
    "uart_open_check_timeout",
    "auto_refresh_timeout",
    "action_open_analyser_tool_proccess",
    "uart_recv_from_widget",
    "data",
    "uart_send_to_widget",
    "table_item_recv_from_widget"
};
#undef QT_MOC_LITERAL
#endif // !QT_MOC_HAS_STRING_DATA
} // unnamed namespace

Q_CONSTINIT static const uint qt_meta_data_CLASSMainWindowENDCLASS[] = {

 // content:
      11,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags, initial metatype offsets
       1,    1,   98,    2, 0x06,    1 /* Public */,
       3,    0,  101,    2, 0x06,    3 /* Public */,

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
       4,    0,  102,    2, 0x08,    4 /* Private */,
       5,    0,  103,    2, 0x08,    5 /* Private */,
       6,    0,  104,    2, 0x08,    6 /* Private */,
       7,    0,  105,    2, 0x08,    7 /* Private */,
       8,    0,  106,    2, 0x08,    8 /* Private */,
       9,    0,  107,    2, 0x08,    9 /* Private */,
      10,    0,  108,    2, 0x08,   10 /* Private */,
      11,    0,  109,    2, 0x08,   11 /* Private */,
      12,    0,  110,    2, 0x08,   12 /* Private */,
      13,    1,  111,    2, 0x08,   13 /* Private */,
      15,    1,  114,    2, 0x08,   15 /* Private */,
      16,    0,  117,    2, 0x08,   17 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,   14,
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void,

       0        // eod
};

Q_CONSTINIT const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_CLASSMainWindowENDCLASS.offsetsAndSizes,
    qt_meta_data_CLASSMainWindowENDCLASS,
    qt_static_metacall,
    nullptr,
    qt_incomplete_metaTypeArray<qt_meta_stringdata_CLASSMainWindowENDCLASS_t,
        // Q_OBJECT / Q_GADGET
        QtPrivate::TypeAndForceComplete<MainWindow, std::true_type>,
        // method 'uart_send_to_widget_signal'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<QByteArray, std::false_type>,
        // method 'manual_refresh_list_signal'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'push_button_clicked_uart_refresh'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'push_button_clicked_uart_open'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'push_button_clicked_manual_refresh'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'check_box_clicked_auto_refresh'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'uart_read_data'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'uart_recv_timeout'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'uart_open_check_timeout'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'auto_refresh_timeout'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'action_open_analyser_tool_proccess'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'uart_recv_from_widget'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<QByteArray, std::false_type>,
        // method 'uart_send_to_widget'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<QByteArray, std::false_type>,
        // method 'table_item_recv_from_widget'
        QtPrivate::TypeAndForceComplete<void, std::false_type>
    >,
    nullptr
} };

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->uart_send_to_widget_signal((*reinterpret_cast< std::add_pointer_t<QByteArray>>(_a[1]))); break;
        case 1: _t->manual_refresh_list_signal(); break;
        case 2: _t->push_button_clicked_uart_refresh(); break;
        case 3: _t->push_button_clicked_uart_open(); break;
        case 4: _t->push_button_clicked_manual_refresh(); break;
        case 5: _t->check_box_clicked_auto_refresh(); break;
        case 6: _t->uart_read_data(); break;
        case 7: _t->uart_recv_timeout(); break;
        case 8: _t->uart_open_check_timeout(); break;
        case 9: _t->auto_refresh_timeout(); break;
        case 10: _t->action_open_analyser_tool_proccess(); break;
        case 11: _t->uart_recv_from_widget((*reinterpret_cast< std::add_pointer_t<QByteArray>>(_a[1]))); break;
        case 12: _t->uart_send_to_widget((*reinterpret_cast< std::add_pointer_t<QByteArray>>(_a[1]))); break;
        case 13: _t->table_item_recv_from_widget(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (MainWindow::*)(QByteArray );
            if (_t _q_method = &MainWindow::uart_send_to_widget_signal; *reinterpret_cast<_t *>(_a[1]) == _q_method) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (MainWindow::*)();
            if (_t _q_method = &MainWindow::manual_refresh_list_signal; *reinterpret_cast<_t *>(_a[1]) == _q_method) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CLASSMainWindowENDCLASS.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType();
        _id -= 14;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::uart_send_to_widget_signal(QByteArray _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MainWindow::manual_refresh_list_signal()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}
QT_WARNING_POP
