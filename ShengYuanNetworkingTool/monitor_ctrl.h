#ifndef MONITOR_CTRL_H
#define MONITOR_CTRL_H

typedef enum
{
    E_CONFIG_LOACL_MODE = 0,
    E_CONFIG_REMOTE_MODE = 1,
    E_CONFIG_BROADCAST_MODE = 2,
}E_CONFIG_MODE;

class MonitorControl
{
public:
    MonitorControl();
    E_CONFIG_MODE eConfigMode;
};

#endif // MONITOR_CTRL_H
