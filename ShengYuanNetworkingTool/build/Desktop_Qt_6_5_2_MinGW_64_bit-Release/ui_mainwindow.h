/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.5.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionfenxigongju;
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_5;
    QVBoxLayout *verticalLayout_2;
    QComboBox *comboBox_uartBaudRate;
    QComboBox *comboBox_uartPort;
    QPushButton *pushButton_uartRefresh;
    QPushButton *pushButton_uartOpen;
    QSpacerItem *verticalSpacer;
    QVBoxLayout *verticalLayout_4;
    QPushButton *pushButton_manual_refresh;
    QCheckBox *checkBox_auto_refresh;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_auto_refresh;
    QLineEdit *lineEdit_auto_refresh;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_networking_config;
    QTableView *tableView_networking;
    QMenuBar *menubar;
    QMenu *menu;
    QMenu *analyse;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName("MainWindow");
        MainWindow->resize(783, 469);
        actionfenxigongju = new QAction(MainWindow);
        actionfenxigongju->setObjectName("actionfenxigongju");
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName("centralwidget");
        horizontalLayout_3 = new QHBoxLayout(centralwidget);
        horizontalLayout_3->setObjectName("horizontalLayout_3");
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName("verticalLayout_5");
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName("verticalLayout_2");
        comboBox_uartBaudRate = new QComboBox(centralwidget);
        comboBox_uartBaudRate->setObjectName("comboBox_uartBaudRate");
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(comboBox_uartBaudRate->sizePolicy().hasHeightForWidth());
        comboBox_uartBaudRate->setSizePolicy(sizePolicy);
        comboBox_uartBaudRate->setMinimumSize(QSize(80, 40));

        verticalLayout_2->addWidget(comboBox_uartBaudRate);

        comboBox_uartPort = new QComboBox(centralwidget);
        comboBox_uartPort->setObjectName("comboBox_uartPort");
        sizePolicy.setHeightForWidth(comboBox_uartPort->sizePolicy().hasHeightForWidth());
        comboBox_uartPort->setSizePolicy(sizePolicy);
        comboBox_uartPort->setMinimumSize(QSize(80, 40));

        verticalLayout_2->addWidget(comboBox_uartPort);

        pushButton_uartRefresh = new QPushButton(centralwidget);
        pushButton_uartRefresh->setObjectName("pushButton_uartRefresh");
        sizePolicy.setHeightForWidth(pushButton_uartRefresh->sizePolicy().hasHeightForWidth());
        pushButton_uartRefresh->setSizePolicy(sizePolicy);
        pushButton_uartRefresh->setMinimumSize(QSize(80, 40));

        verticalLayout_2->addWidget(pushButton_uartRefresh);

        pushButton_uartOpen = new QPushButton(centralwidget);
        pushButton_uartOpen->setObjectName("pushButton_uartOpen");
        sizePolicy.setHeightForWidth(pushButton_uartOpen->sizePolicy().hasHeightForWidth());
        pushButton_uartOpen->setSizePolicy(sizePolicy);
        pushButton_uartOpen->setMinimumSize(QSize(80, 40));

        verticalLayout_2->addWidget(pushButton_uartOpen);


        verticalLayout_5->addLayout(verticalLayout_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName("verticalLayout_4");
        pushButton_manual_refresh = new QPushButton(centralwidget);
        pushButton_manual_refresh->setObjectName("pushButton_manual_refresh");
        sizePolicy.setHeightForWidth(pushButton_manual_refresh->sizePolicy().hasHeightForWidth());
        pushButton_manual_refresh->setSizePolicy(sizePolicy);
        pushButton_manual_refresh->setMinimumSize(QSize(80, 40));

        verticalLayout_4->addWidget(pushButton_manual_refresh);

        checkBox_auto_refresh = new QCheckBox(centralwidget);
        checkBox_auto_refresh->setObjectName("checkBox_auto_refresh");
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(checkBox_auto_refresh->sizePolicy().hasHeightForWidth());
        checkBox_auto_refresh->setSizePolicy(sizePolicy1);
        checkBox_auto_refresh->setCheckable(true);

        verticalLayout_4->addWidget(checkBox_auto_refresh);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName("horizontalLayout_2");
        label_auto_refresh = new QLabel(centralwidget);
        label_auto_refresh->setObjectName("label_auto_refresh");
        sizePolicy1.setHeightForWidth(label_auto_refresh->sizePolicy().hasHeightForWidth());
        label_auto_refresh->setSizePolicy(sizePolicy1);

        horizontalLayout_2->addWidget(label_auto_refresh);

        lineEdit_auto_refresh = new QLineEdit(centralwidget);
        lineEdit_auto_refresh->setObjectName("lineEdit_auto_refresh");
        lineEdit_auto_refresh->setEnabled(true);
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(lineEdit_auto_refresh->sizePolicy().hasHeightForWidth());
        lineEdit_auto_refresh->setSizePolicy(sizePolicy2);
        lineEdit_auto_refresh->setMaximumSize(QSize(70, 16777215));

        horizontalLayout_2->addWidget(lineEdit_auto_refresh);


        verticalLayout_4->addLayout(horizontalLayout_2);


        verticalLayout_5->addLayout(verticalLayout_4);


        horizontalLayout_3->addLayout(verticalLayout_5);

        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName("groupBox_2");
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy3);
        groupBox_2->setMinimumSize(QSize(631, 301));
        gridLayout = new QGridLayout(groupBox_2);
        gridLayout->setObjectName("gridLayout");
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName("verticalLayout");
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName("horizontalLayout");
        label = new QLabel(groupBox_2);
        label->setObjectName("label");

        horizontalLayout->addWidget(label);

        horizontalSpacer = new QSpacerItem(37, 17, QSizePolicy::Preferred, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_networking_config = new QPushButton(groupBox_2);
        pushButton_networking_config->setObjectName("pushButton_networking_config");
        sizePolicy1.setHeightForWidth(pushButton_networking_config->sizePolicy().hasHeightForWidth());
        pushButton_networking_config->setSizePolicy(sizePolicy1);
        pushButton_networking_config->setMinimumSize(QSize(0, 0));

        horizontalLayout->addWidget(pushButton_networking_config);


        verticalLayout->addLayout(horizontalLayout);

        tableView_networking = new QTableView(groupBox_2);
        tableView_networking->setObjectName("tableView_networking");

        verticalLayout->addWidget(tableView_networking);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);


        horizontalLayout_3->addWidget(groupBox_2);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName("menubar");
        menubar->setGeometry(QRect(0, 0, 783, 20));
        menu = new QMenu(menubar);
        menu->setObjectName("menu");
        analyse = new QMenu(menubar);
        analyse->setObjectName("analyse");
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName("statusbar");
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menu->menuAction());
        menubar->addAction(analyse->menuAction());

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        actionfenxigongju->setText(QCoreApplication::translate("MainWindow", "\345\210\206\346\236\220\345\267\245\345\205\267", nullptr));
        pushButton_uartRefresh->setText(QCoreApplication::translate("MainWindow", "\345\210\267\346\226\260\344\270\262\345\217\243", nullptr));
        pushButton_uartOpen->setText(QCoreApplication::translate("MainWindow", "\346\211\223\345\274\200\344\270\262\345\217\243", nullptr));
        pushButton_manual_refresh->setText(QCoreApplication::translate("MainWindow", "\346\211\213\345\212\250\346\225\260\346\215\256\345\210\267\346\226\260", nullptr));
        checkBox_auto_refresh->setText(QCoreApplication::translate("MainWindow", "\350\207\252\345\212\250\345\210\267\346\226\260\344\275\277\350\203\275", nullptr));
        label_auto_refresh->setText(QCoreApplication::translate("MainWindow", "\350\207\252\345\212\250\345\210\267\346\226\260\351\227\264\351\232\224(s)", nullptr));
        groupBox_2->setTitle(QString());
        label->setText(QCoreApplication::translate("MainWindow", "\346\225\264\347\253\231\350\256\276\345\244\207\344\277\241\346\201\257\346\246\202\350\247\210", nullptr));
        pushButton_networking_config->setText(QCoreApplication::translate("MainWindow", "\351\205\215\347\275\256\346\223\215\344\275\234", nullptr));
        menu->setTitle(QCoreApplication::translate("MainWindow", "\350\217\234\345\215\225", nullptr));
        analyse->setTitle(QCoreApplication::translate("MainWindow", "\345\210\206\346\236\220", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
