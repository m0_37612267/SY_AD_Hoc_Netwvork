#include <QMessageBox>
#include "networking_control.h"
#include "system_config.h"
#include "ui_system_config.h"


/*组网相关的ID列表限制控制*/
NetworkingControl::NetworkingControl()
{
    node_list_total_num = 0;
}

/*清除ID列表*/
void NetworkingControl::clear_node_list(void)
{
    node_list_total_num = 0;
}

/*获取指定ID的参数*/
void NetworkingControl::get_node_param(sNodeListParam* param)
{
    for(uint8_t cnt = 0; cnt < node_list_total_num; cnt++)
    {
        if(nodeListParam[cnt].id == param->id)
        {
            /*找到匹配ID，获取对应参数*/
            param->type = nodeListParam[cnt].type;
            param->version = nodeListParam[cnt].version;
            param->state = nodeListParam[cnt].state;
            param->rssi = nodeListParam[cnt].rssi;
            return;
        }
    }
}

/*更新指定ID的参数至ID列表，没有ID则添加*/
void NetworkingControl::update_node_param(sNodeListParam* param)
{
    for(uint8_t cnt = 0; cnt < node_list_total_num; cnt++)
    {
        if(nodeListParam[cnt].id == param->id)
        {
            /*找到匹配ID，配置对应参数*/
            if(0 != param->type.compare("不修改"))
            {
                nodeListParam[cnt].type = param->type;
            }
            if(0 != param->version.compare("不修改"))
            {
                nodeListParam[cnt].version = param->version;
            }
            if(0 != param->state.compare("不修改"))
            {
                nodeListParam[cnt].state = param->state;
            }
            if(0 != param->rssi.compare("不修改"))
            {
                nodeListParam[cnt].rssi = param->rssi;
            }
            return;
        }
    }

    /*未找到匹配ID，新建一行*/
    if(node_list_total_num > NODELIST_MAX_NUM)
    {
        /*列表溢出则不支持新建*/
        return;
    }

    nodeListParam[node_list_total_num].id = param->id;
    if(0 != param->type.compare("不修改"))
    {
        nodeListParam[node_list_total_num].type = param->type;
    }
    else
    {
        nodeListParam[node_list_total_num].type.resize(0);
    }
    if(0 != param->version.compare("不修改"))
    {
        nodeListParam[node_list_total_num].version = param->version;
    }
    else
    {
        nodeListParam[node_list_total_num].version.resize(0);
    }
    if(0 != param->state.compare("不修改"))
    {
        nodeListParam[node_list_total_num].state = param->state;
    }
    else
    {
        nodeListParam[node_list_total_num].state.resize(0);
    }
    if(0 != param->rssi.compare("不修改"))
    {
        nodeListParam[node_list_total_num].rssi = param->rssi;
    }
    else
    {
        nodeListParam[node_list_total_num].rssi.resize(0);
    }
    node_list_total_num++;

    sort_list();
}

/*添加指定ID至ID列表，没有附带参数*/
void NetworkingControl::add_node(QByteArray id)
{
    if(node_list_total_num > NODELIST_MAX_NUM)
    {
        /*列表溢出则不支持新建*/
        return;
    }

    /*ID查重*/
    for(uint8_t cnt = 0; cnt < node_list_total_num; cnt++)
    {
        if(nodeListParam[cnt].id == id)
        {
            /*已存在相同ID，不再添加*/
            qDebug() << "add_node: same id";
            nodeListParam[cnt].state = "已添加";
            return;
        }
    }

    nodeListParam[node_list_total_num].id = id;
    nodeListParam[node_list_total_num].type.resize(0);
    nodeListParam[node_list_total_num].version.resize(0);
    nodeListParam[node_list_total_num].state.resize(0);
    nodeListParam[node_list_total_num].state.append("已添加");
    nodeListParam[node_list_total_num].rssi.resize(0);
    node_list_total_num++;

    sort_list();
}

/*删除ID列表的指定ID*/
void NetworkingControl::delete_node(QByteArray id)
{
    for(uint8_t cnt = 0; cnt < node_list_total_num; cnt++)
    {
        if(nodeListParam[cnt].id == id)
        {
            qDebug() << "delete_node: find id" << cnt;
            /*找到匹配ID，删除对应参数*/
            for(uint8_t replace_index = cnt; (replace_index + 1) < node_list_total_num; replace_index++)
            {
                nodeListParam[replace_index].id = nodeListParam[replace_index + 1].id;
                nodeListParam[replace_index].type = nodeListParam[replace_index + 1].type;
                nodeListParam[replace_index].version = nodeListParam[replace_index + 1].version;
                nodeListParam[replace_index].state = nodeListParam[replace_index + 1].state;
                nodeListParam[replace_index].rssi = nodeListParam[replace_index + 1].rssi;
                qDebug() << "delete_node: replace" << replace_index << nodeListParam[replace_index].id;
            }
            node_list_total_num--;
            sort_list();
            return;
        }
    }
}

/*ID列表的排序，将已组网的放最前面*/
void NetworkingControl::sort_list(void)
{
    sNodeListParam temp;

    for(uint8_t i = 0; i < node_list_total_num; i++)
    {
        int8_t j = i - 1;
        /*QByteArray不能直接进行内存操作*/
        //memcpy((uint8_t*)&temp, (uint8_t*)&nodeListParam[i], sizeof(sNodeListParam));
        temp.id = nodeListParam[i].id;
        temp.type = nodeListParam[i].type;
        temp.version = nodeListParam[i].version;
        temp.state = nodeListParam[i].state;
        temp.rssi = nodeListParam[i].rssi;

        while((j >= 0) && (("已添加" == nodeListParam[i].state) && ("已添加" != nodeListParam[j].state)))
        {
            //memcpy((uint8_t*)&nodeListParam[j+1], (uint8_t*)&nodeListParam[j], sizeof(sNodeListParam));
            nodeListParam[j+1].id = nodeListParam[j].id;
            nodeListParam[j+1].type = nodeListParam[j].type;
            nodeListParam[j+1].version = nodeListParam[j].version;
            nodeListParam[j+1].state = nodeListParam[j].state;
            nodeListParam[j+1].rssi = nodeListParam[j].rssi;
            j = j - 1;
        }

        //memcpy((uint8_t*)&nodeListParam[j+1], (uint8_t*)&temp, sizeof(sNodeListParam));
        nodeListParam[j+1].id = temp.id;
        nodeListParam[j+1].type = temp.type;
        nodeListParam[j+1].version = temp.version;
        nodeListParam[j+1].state = temp.state;
        nodeListParam[j+1].rssi = temp.rssi;
    }
}


void NetworkingControl::table_show(QStandardItemModel* Item)
{
    for(uint8_t row_id = 0; row_id < node_list_total_num; row_id++)
    {
        while(Item->rowCount() < row_id)
        {
            Item->insertRow(Item->rowCount());
        }
        Item->setItem(row_id, 0, new QStandardItem(nodeListParam[row_id].id));
        Item->setItem(row_id, 1, new QStandardItem(nodeListParam[row_id].type));
        Item->setItem(row_id, 2, new QStandardItem(nodeListParam[row_id].version));
        Item->setItem(row_id, 3, new QStandardItem(nodeListParam[row_id].state));
        Item->setItem(row_id, 4, new QStandardItem(nodeListParam[row_id].rssi));
    }
}


/*扫描设备*/
void SystemConfig::push_button_clicked_scan_device(void)
{
    atPrtcl.assemble_cmd = E_AT_SCAN_NODE;
    atPrtcl.assemble_buff.resize(0);
    atPrtcl.analyse_cmd = atPrtcl.assemble_cmd;
    atPrtcl.analyse_id.resize(0);

    if(TRUE == atPrtcl.at_protocol_assemble(atPrtcl.assemble_cmd, &atPrtcl.assemble_buff, &atPrtcl.assemble_buff))
    {
        uart_send_to_main_window(atPrtcl.assemble_buff);
    }
}

/*查看设备列表*/
void SystemConfig::push_button_clicked_check_device_list(void)
{
    atPrtcl.assemble_cmd = E_AT_GET_NODELIST;
    atPrtcl.assemble_buff.resize(0);
    atPrtcl.analyse_cmd = atPrtcl.assemble_cmd;
    atPrtcl.analyse_id.resize(0);

    if(TRUE == atPrtcl.at_protocol_assemble(atPrtcl.assemble_cmd, &atPrtcl.assemble_buff, &atPrtcl.assemble_buff))
    {
        uart_send_to_main_window(atPrtcl.assemble_buff);
    }
}

/*添加设备*/
void SystemConfig::push_button_clicked_add_device(void)
{
    if(0 == ui->lineEdit_destId->text().length())
    {
        QMessageBox::critical(this, "错误信息", "目标ID为空，无法执行添加设备操作");
        return;
    }
    atPrtcl.assemble_cmd = E_AT_ADD_NODE;
    atPrtcl.assemble_buff.resize(0);
    atPrtcl.assemble_buff.append(ui->lineEdit_destId->text().toUtf8());
    atPrtcl.analyse_cmd = atPrtcl.assemble_cmd;
    atPrtcl.analyse_id.resize(0);
    atPrtcl.analyse_id.append(ui->lineEdit_destId->text().toUtf8());

    if(TRUE == atPrtcl.at_protocol_assemble(atPrtcl.assemble_cmd, &atPrtcl.assemble_buff, &atPrtcl.assemble_buff))
    {
        uart_send_to_main_window(atPrtcl.assemble_buff);
    }
}

/*删除设备*/
void SystemConfig::push_button_clicked_delete_device(void)
{
    if(0 == ui->lineEdit_destId->text().length())
    {
        QMessageBox::critical(this, "错误信息", "目标ID为空，无法执行删除设备操作");
        return;
    }
    atPrtcl.assemble_cmd = E_AT_DELETE_NODE;
    atPrtcl.assemble_buff.resize(0);
    atPrtcl.assemble_buff.append(ui->lineEdit_destId->text().toUtf8());
    atPrtcl.analyse_cmd = atPrtcl.assemble_cmd;
    atPrtcl.analyse_id.resize(0);
    atPrtcl.analyse_id.append(ui->lineEdit_destId->text().toUtf8());

    if(TRUE == atPrtcl.at_protocol_assemble(atPrtcl.assemble_cmd, &atPrtcl.assemble_buff, &atPrtcl.assemble_buff))
    {
        uart_send_to_main_window(atPrtcl.assemble_buff);
    }
}

