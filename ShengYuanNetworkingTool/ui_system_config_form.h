/********************************************************************************
** Form generated from reading UI file 'system_config_form.ui'
**
** Created by: Qt User Interface Compiler version 6.5.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SYSTEM_CONFIG_FORM_H
#define UI_SYSTEM_CONFIG_FORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_System_Config_Form
{
public:
    QGroupBox *groupBox;

    void setupUi(QWidget *System_Config_Form)
    {
        if (System_Config_Form->objectName().isEmpty())
            System_Config_Form->setObjectName("System_Config_Form");
        System_Config_Form->resize(640, 480);
        groupBox = new QGroupBox(System_Config_Form);
        groupBox->setObjectName("groupBox");
        groupBox->setGeometry(QRect(180, 130, 120, 80));

        retranslateUi(System_Config_Form);

        QMetaObject::connectSlotsByName(System_Config_Form);
    } // setupUi

    void retranslateUi(QWidget *System_Config_Form)
    {
        System_Config_Form->setWindowTitle(QCoreApplication::translate("System_Config_Form", "Form", nullptr));
        groupBox->setTitle(QCoreApplication::translate("System_Config_Form", "GroupBox", nullptr));
    } // retranslateUi

};

namespace Ui {
    class System_Config_Form: public Ui_System_Config_Form {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SYSTEM_CONFIG_FORM_H
