#include "bsp_timer.h"

/*构建函数*/
BspTimerControl::BspTimerControl(void)
{
    serial_recv_timer = new QTimer;
    serial_open_check_timer = new QTimer;
    auto_refresh_timer = new QTimer;
    update_ack_timer = new QTimer;
    update_state_timer = new QTimer;
}
/*析构函数*/
BspTimerControl::~BspTimerControl(void)
{
    delete serial_recv_timer;
    delete serial_open_check_timer;
    delete auto_refresh_timer;
    delete update_ack_timer;
    delete update_state_timer;
}

/*获取当前系统时间*/
QString BspTimerControl::get_current_timestamp(void)
{
    return QString(QTime::currentTime().toString("HH_mm_ss_zzz"));
}

/*开启定时器*/
void BspTimerControl::start_timer(QTimer* timer, int delay)
{
    timer->start(delay);
}

/*关闭定时器*/
void BspTimerControl::stop_timer(QTimer* timer)
{
    timer->stop();
}
