#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "bsp_file.h"
#include "bsp_timer.h"
#include "bsp_uart.h"
#include "uart_trsprt.h"
#include "system_config.h"


#ifndef TRUE
#define TRUE (1)
#endif

#ifndef FALSE
#define FALSE (0)
#endif

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    /*主窗口对象*/
    Ui::MainWindow *ui;
    /*子窗口对象*/
    SystemConfig *sys_config_widget = new SystemConfig;
    /*链路驱动相关对象*/
    BspFileControl bspFileCtrl;
    BspTimerControl bspTimerCtrl;
    BspSerialControl bspSerialCtrl;
    /*串口数据结构*/
    SerialTrsprtControl serialTrsprtCtrl;
    /*设备列表输出表格*/
    QStandardItemModel* main_devicelist_item_model;
    /*自动刷新列表的变量控制*/
    uint32_t auto_refresh_time;
    uint32_t auto_refresh_timeout_cnt;

    /*串口初始化*/
    void serial_init(void);
    /*主窗口界面初始化*/
    void main_window_init(void);

protected:
    void resizeEvent(QResizeEvent *event);

private slots:
    /*串口响应*/
    void push_button_clicked_uart_refresh(void);
    void push_button_clicked_uart_open(void);
    void push_button_clicked_manual_refresh(void);
    void check_box_clicked_auto_refresh(void);
    void uart_read_data(void);
    void uart_recv_timeout(void);
    void uart_open_check_timeout(void);
    void auto_refresh_timeout(void);
    /*菜单响应*/
    void action_open_analyser_tool_proccess(void);
    /*子窗口数据交互*/
    void uart_recv_from_widget(QByteArray data);
    void uart_send_to_widget(QByteArray);
    void table_item_recv_from_widget(void);

signals:
    /*子窗口数据交互*/
    void uart_send_to_widget_signal(QByteArray);
    void manual_refresh_list_signal(void);

};
#endif // MAINWINDOW_H
