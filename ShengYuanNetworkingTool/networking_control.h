#ifndef NETWORKING_CONTROL_H
#define NETWORKING_CONTROL_H

#include <QByteArray>
#include <QStandardItemModel>

#define NODELIST_MAX_NUM (32)

typedef struct
{
    QByteArray id;
    QByteArray type;
    QByteArray version;
    QByteArray state;
    QByteArray rssi;
}sNodeListParam;

class NetworkingControl
{
public:
    NetworkingControl();
    uint8_t node_list_total_num;
    sNodeListParam nodeListParam[NODELIST_MAX_NUM];
    void clear_node_list(void);
    void get_node_param(sNodeListParam* param);
    void update_node_param(sNodeListParam* param);
    void add_node(QByteArray id);
    void delete_node(QByteArray id);
    void sort_list(void);
    void table_show(QStandardItemModel* Item);
};

#endif // NETWORKING_CONTROL_H
