#include "bsp_uart.h"

/*构建函数*/
BspSerialControl::BspSerialControl(void)
{
    serial = new QSerialPort;
}
/*析构函数*/
BspSerialControl::~BspSerialControl(void)
{
    delete serial;
}
/*遍历串口端口，返回端口号*/
QStringList BspSerialControl::search_serial_port(void)
{
    QStringList portBuff;
    foreach(const QSerialPortInfo &info,QSerialPortInfo::availablePorts())
    {
        QSerialPort serial;
        serial.setPort(info);
        if(serial.open(QIODevice::ReadWrite))
        {
            portBuff.append(serial.portName());
            serial.close();
        }
    }
    return portBuff;
}
/*打开指定串口*/
void BspSerialControl::serial_open(QString strCom, QString StrBaud)
{
    /*设置串口名*/
    serial->setPortName(strCom);
    /*打开串口*/
    serial->open(QIODevice::ReadWrite);
    /*设置波特率*/
    serial->setBaudRate(StrBaud.toULong());
    /*设置数据位数*/
    serial->setDataBits(QSerialPort::Data8);
    /*设置校验位*/
    serial->setParity(QSerialPort::NoParity);
    /*设置停止位*/
    serial->setStopBits(QSerialPort::OneStop);
    /*设置为无流控制*/
    serial->setFlowControl(QSerialPort::NoFlowControl);
}
/*关闭串口*/
void BspSerialControl::serial_close(void)
{
    serial->clear();
    serial->close();
}
/*检查串口异常状态*/
QSerialPort::SerialPortError BspSerialControl::get_serial_err(void)
{
    return serial->error();
}
/*写串口*/
void BspSerialControl::serial_write(QByteArray ArData)
{
    serial->write(ArData);
}
/*读串口*/
QByteArray BspSerialControl::serial_read(void)
{
    return serial->readAll();
}

