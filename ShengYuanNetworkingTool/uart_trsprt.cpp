#include <QMessageBox>
#include "mainwindow.h"
#include "ui_mainwindow.h"



/*主窗口串口数据初始化*/
void MainWindow::serial_init(void)
{
    /*遍历COM口*/
    QStringList portBuff = bspSerialCtrl.search_serial_port();
    foreach (const QString &info, portBuff)
    {
        ui->comboBox_uartPort->addItem(info);
    }

    /*配置波特率,默认115200*/
    ui->comboBox_uartBaudRate->addItem("9600");
    ui->comboBox_uartBaudRate->addItem("38400");
    ui->comboBox_uartBaudRate->addItem("115200");
    ui->comboBox_uartBaudRate->addItem("125000");
    ui->comboBox_uartBaudRate->addItem("230400");
    ui->comboBox_uartBaudRate->addItem("460800");
    ui->comboBox_uartBaudRate->addItem("912600");
    if(true == bspFileCtrl.is_setting_exist(E_SETTING_UART_BAUDRATE))
    {
        ui->comboBox_uartBaudRate->setCurrentIndex(bspFileCtrl.get_setting(E_SETTING_UART_BAUDRATE).toInt());
    }
    else
    {
        ui->comboBox_uartBaudRate->setCurrentIndex(2);
    }
}

/*按下"刷新串口"按键*/
void MainWindow::push_button_clicked_uart_refresh(void)
{
    /*遍历COM口*/
    QStringList portBuff = bspSerialCtrl.search_serial_port();
    ui->comboBox_uartPort->clear();
    foreach (const QString &info, portBuff)
    {
        ui->comboBox_uartPort->addItem(info);
    }
}

/*按下"打开串口"按键*/
void MainWindow::push_button_clicked_uart_open(void)
{
    if("打开串口" == ui->pushButton_uartOpen->text())
    {
        /*ui显示变化*/
        ui->pushButton_uartOpen->setText("关闭串口");
        ui->comboBox_uartPort->setEnabled(false);
        ui->comboBox_uartBaudRate->setEnabled(false);
        ui->pushButton_uartRefresh->setEnabled(false);
        bspFileCtrl.save_setting(E_SETTING_UART_BAUDRATE,ui->comboBox_uartBaudRate->currentIndex());
        /*打开串口，开启串口有效性检测*/
        bspSerialCtrl.serial_open(ui->comboBox_uartPort->currentText(), ui->comboBox_uartBaudRate->currentText());
        bspTimerCtrl.start_timer(bspTimerCtrl.serial_open_check_timer, SERIAL_OPEN_CHECK_TIMEOUT);
    }
    else
    {
        /*ui显示变化*/
        ui->pushButton_uartOpen->setText("打开串口");
        ui->comboBox_uartPort->setEnabled(true);
        ui->comboBox_uartBaudRate->setEnabled(true);
        ui->pushButton_uartRefresh->setEnabled(true);
        /*关闭串口，关闭串口有效性检测*/
        bspSerialCtrl.serial_close();
        bspTimerCtrl.stop_timer(bspTimerCtrl.serial_open_check_timer);
    }
}

/*读取串口数据，开启接收超时*/
void MainWindow::uart_read_data(void)
{
    if(serialTrsprtCtrl.serial_recv_buf.isEmpty())
    {
        /*第一次收数据时，记录时间戳*/
        serialTrsprtCtrl.serial_recv_time = QString("[%1]:RX->").arg(BspTimerControl::get_current_timestamp());
    }
    serialTrsprtCtrl.serial_recv_buf.append(bspSerialCtrl.serial_read());

    // if(serialTrsprtCtrl.serial_recv_buf[serialTrsprtCtrl.serial_recv_buf.size()-1] == 0x7F)
    // {
    //     /*如果可以提前判断接收完成，则不需要继续等待超时直接解析*/
    //     uart_recv_timeout();
    // }
    // else
    {
        bspTimerCtrl.start_timer(bspTimerCtrl.serial_recv_timer, SERIAL_RECV_TIMEOUT);
    }
}

/*串口接收超时，判断数据帧有效性（暂不考虑合包拆包）*/
void MainWindow::uart_recv_timeout(void)
{
    bspTimerCtrl.stop_timer(bspTimerCtrl.serial_recv_timer);
    if(!serialTrsprtCtrl.serial_recv_buf.isEmpty())
    {
        qDebug() << "rcv" << serialTrsprtCtrl.serial_recv_buf.toHex(' ').trimmed().toUpper() << tr(serialTrsprtCtrl.serial_recv_buf);

        uart_send_to_widget(serialTrsprtCtrl.serial_recv_buf);

        // if(E_SERIAL_ASCII_MODE == eWorkMode)
        // {
        //     /*ASCII模式数据直接回显*/
        //     QString str;
        //     str = ("<font color=blue>" + serialTrsprtCtrl.serial_recv_time + "<font color=black>"
        //            + tr(serialTrsprtCtrl.serial_recv_buf));
        //     ui->textEdit_uart->append(str);
        //     /*存储日志*/
        //     bspFileCtrl.write_run_log(QtInfoMsg, bspTimerCtrl.get_current_timestamp(),"uart recv "+str);
        //     /*如果子窗口开启，则将数据交由子窗口进行解析*/
        //     if(true == form->isVisible())
        //     {
        //         emit send_form_data(serialTrsprtCtrl.serial_recv_buf);
        //     }
        // }
        // else if(E_SERIAL_HEX_MODE == eWorkMode)
        // {
        //     /*HEX数据需转换为ASCII再回显*/
        //     QString str;
        //     str = ("<font color=blue>" + serialTrsprtCtrl.serial_recv_time +
        //            + "raw: <font color=black>" + tr(serialTrsprtCtrl.serial_recv_buf.trimmed())
        //            + '\n' + "<font color=blue>" + "hex: " + "<font color=black>"
        //            + serialTrsprtCtrl.serial_recv_buf.toHex(' ').trimmed().toUpper()
        //            + '\n');

        //     ui->textEdit_uart->append(str);
        //     /*存储日志*/
        //     bspFileCtrl.write_run_log(QtInfoMsg, bspTimerCtrl.get_current_timestamp(),"uart recv "+str);
        //     /*如果子窗口开启，则将数据交由子窗口进行解析*/
        //     if(true == form->isVisible())
        //     {
        //         emit send_form_data(serialTrsprtCtrl.serial_recv_buf);
        //     }
        // }
        // else if((E_LOCAL_UPDATE_MODE == eWorkMode)
        //          || (E_REMOTE_UPDATE_MODE == eWorkMode))
        // {
        //     /*升级模式需要交给升级状态机进行协议解析*/
        //     qDebug() << "rcv" << serialTrsprtCtrl.serial_recv_buf;
        //     if('r' == serialTrsprtCtrl.serial_recv_buf.at(0))
        //     {

        //     }
        //     else if(0 == updateCtrl.updateRecvFlg)
        //     {
        //         updateCtrl.recvPack = serialTrsprtCtrl.serial_recv_buf;
        //         updateCtrl.updateRecvFlg = 1;
        //     }
        // }
    }

    serialTrsprtCtrl.serial_recv_buf.clear();
}

/*定时检查串口有效性*/
void MainWindow::uart_open_check_timeout(void)
{
    if(QSerialPort::SerialPortError::NoError != bspSerialCtrl.get_serial_err())
    {
        /*串口出现错误，提示串口异常*/
        ui->pushButton_uartOpen->setText("打开串口");
        ui->comboBox_uartPort->setEnabled(true);
        ui->comboBox_uartBaudRate->setEnabled(true);
        ui->pushButton_uartRefresh->setEnabled(true);
        /*关闭串口，关闭串口有效性检测*/
        bspSerialCtrl.serial_close();
        bspTimerCtrl.stop_timer(bspTimerCtrl.serial_open_check_timer);
        QMessageBox::critical(this, "错误信息", "串口异常，请检查连接");
    }
    else
    {
        bspTimerCtrl.start_timer(bspTimerCtrl.serial_open_check_timer, SERIAL_OPEN_CHECK_TIMEOUT);
    }
}
