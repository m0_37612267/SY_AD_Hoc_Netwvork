#ifndef BSP_TIMER_H
#define BSP_TIMER_H

#include <QTimer>
#include <QTime>

#define SERIAL_RECV_TIMEOUT (20)
#define SERIAL_OPEN_CHECK_TIMEOUT (5000)


class BspTimerControl
{
public:
    BspTimerControl(void);
    ~BspTimerControl(void);
    QTimer* serial_recv_timer;
    QTimer* serial_open_check_timer;
    QTimer* auto_refresh_timer;
    QTimer* update_ack_timer;
    QTimer* update_state_timer;
    static QString get_current_timestamp(void);
    void start_timer(QTimer*, int);
    void stop_timer(QTimer*);
private:

};

#endif // BSP_TIMER_H
