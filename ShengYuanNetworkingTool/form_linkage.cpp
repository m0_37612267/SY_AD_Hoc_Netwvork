#include "mainwindow.h"
#include "system_config.h"
#include "ui_mainwindow.h"
#include "ui_system_config.h"


/*打开子窗口界面*/
void MainWindow::action_open_analyser_tool_proccess(void)
{
    sys_config_widget->show();
}

/*主窗口接收到子窗口的串口数据*/
void MainWindow::uart_recv_from_widget(QByteArray data)
{
    qDebug() << "uart_recv_from_widget" << data;

    bspSerialCtrl.serial_write(data);
    /*主窗口存储日志*/
    QString str = (data);
    bspFileCtrl.write_run_log(QtInfoMsg, bspTimerCtrl.get_current_timestamp(),"TX->"+str);

    //待删，上位机交互功能测试，模拟数据接收解析
    if(Qt::CheckState::Checked == ui->checkBox_function_test->checkState())
    {
        QByteArray byteArray;
        int index = 0;
        int end_index = 0;
        /*找到帧头*/
        while(-1 != (index = data.indexOf("AT", index)))
        {
            // qDebug() << index;
            /*找到帧尾*/
            if(-1 == (end_index = data.indexOf('\r', index)))
            {
                end_index = data.length();
            }
            end_index--;
            // qDebug() << end_index;

            if(1 == (end_index - index))
            {
                /*测试AT启动*/
                qDebug() << "E_AT";
                byteArray = "OK";
                byteArray += '\r';
                byteArray += '\n';
                emit uart_send_to_widget_signal(byteArray);
            }
            else if((12 <= (end_index - index))
                     &&(0 == memcmp(&data[index + 2], "+GETVERSION", 11)))
            {
                /*查询软硬件版本*/
                qDebug() << "E_AT_GET_VERSION";
                byteArray = "ID: 12345678";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "software ver: V01.00.00";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "hardware ver: V01.00.00";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "OK";
                byteArray += '\r';
                byteArray += '\n';
                emit uart_send_to_widget_signal(byteArray);
            }
            else if((7 <= (end_index - index))
                     &&(0 == memcmp(&data[index + 2], "+RESET", 6)))
            {
                /*重启*/
                qDebug() << "E_AT_RESET";
                byteArray = "ID: 12345678";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "OK";
                byteArray += '\r';
                byteArray += '\n';
                emit uart_send_to_widget_signal(byteArray);
            }
            else if((9 <= (end_index - index))
                     &&(0 == memcmp(&data[index + 2], "+RESTORE", 8)))
            {
                /*重置*/
                qDebug() << "E_AT_RESTORE";
                byteArray = "ID: 12345678";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "OK";
                byteArray += '\r';
                byteArray += '\n';
                emit uart_send_to_widget_signal(byteArray);
            }
            else if((10 <= (end_index - index))
                     &&(0 == memcmp(&data[index + 2], "+SCANNODE", 9)))
            {
                /*扫描*/
                qDebug() << "E_AT_SCAN_NODE";
                byteArray = "ID: 12345678";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "RSSI: -70,-77";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "State: 已添加";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "OK";
                byteArray += '\r';
                byteArray += '\n';
                emit uart_send_to_widget_signal(byteArray);

                /*等待50ms*/
                QTime _Timer = QTime::currentTime().addMSecs(50);
                while( QTime::currentTime() < _Timer )
                    QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

                byteArray = "ID: 22345678";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "RSSI: -70,-77";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "State: 未添加";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "OK";
                byteArray += '\r';
                byteArray += '\n';
                emit uart_send_to_widget_signal(byteArray);
            }
            else if((9 <= (end_index - index))
                     &&(0 == memcmp(&data[index + 2], "+ADDNODE", 8)))
            {
                /*添加*/
                qDebug() << "E_AT_ADD_NODE";
                byteArray = "ID: 12345678";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "OK";
                byteArray += '\r';
                byteArray += '\n';
                emit uart_send_to_widget_signal(byteArray);
            }
            else if((12 <= (end_index - index))
                     &&(0 == memcmp(&data[index + 2], "+DELETENODE", 11)))
            {
                /*删除*/
                qDebug() << "E_AT_DELETE_NODE";
                byteArray = "ID: 12345678";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "OK";
                byteArray += '\r';
                byteArray += '\n';
                emit uart_send_to_widget_signal(byteArray);
            }
            else if((11 <= (end_index - index))
                     &&(0 == memcmp(&data[index + 2], "+NODELIST?", 10)))
            {
                /*获取设备列表*/
                qDebug() << "E_AT_GET_NODELIST";
                byteArray = "ID: 12345678";
                byteArray += '\r';
                byteArray += '\n';
                byteArray = "ID: 22345678";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "OK";
                byteArray += '\r';
                byteArray += '\n';
                emit uart_send_to_widget_signal(byteArray);
            }
            else if((12 <= (end_index - index))
                     &&(0 == memcmp(&data[index + 2], "+NODESTATE?", 11)))
            {
                /*获取设备状态*/
                qDebug() << "E_AT_GET_NODE_STATE";
                byteArray = "ID: 12345678";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "Type: 从机";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "Version: V01.00.00,V01.00.00";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "RSSI: -70,-77";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "State: 正常";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "OK";
                byteArray += '\r';
                byteArray += '\n';
                emit uart_send_to_widget_signal(byteArray);
            }
            else if((11 <= (end_index - index))
                     &&(0 == memcmp(&data[index + 2], "+NODESTATE", 10)))
            {
                /*清除设备状态*/
                qDebug() << "E_AT_CLEAR_NODE_STATE";
                byteArray = "ID: 12345678";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "OK";
                byteArray += '\r';
                byteArray += '\n';
                emit uart_send_to_widget_signal(byteArray);
            }
            else if((10 <= (end_index - index))
                     &&(0 == memcmp(&data[index + 2], "+TRANSFER", 9)))
            {
                /*数据透传*/
                qDebug() << "E_AT_TRANSFER";
                byteArray = "ID: 12345678";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "OK";
                byteArray += '\r';
                byteArray += '\n';
                emit uart_send_to_widget_signal(byteArray);
            }
            else if((9 <= (end_index - index))
                     &&(0 == memcmp(&data[index + 2], "+UPDATE", 7)))
            {
                /*升级*/
                qDebug() << "E_AT_UPDATE";
                byteArray = "ID: 12345678";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "Addr: 0000";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "State: 0";
                byteArray += '\r';
                byteArray += '\n';
                byteArray += "OK";
                byteArray += '\r';
                byteArray += '\n';
                emit uart_send_to_widget_signal(byteArray);
            }

            index++;
            if(index >= data.length())
            {
                break;
            }
        }
    }

}

/*主窗口准备传递串口数据给子窗口*/
void MainWindow::uart_send_to_widget(QByteArray data)
{
    /*主窗口日志存储*/
    QString str = (data);
    bspFileCtrl.write_run_log(QtInfoMsg, bspTimerCtrl.get_current_timestamp(),"RX->"+str);

    /*数据传递给子窗口解析*/
    emit uart_send_to_widget_signal(data);
}

/*子窗口接收到主窗口的串口数据*/
void SystemConfig::uart_recv_from_main_window(QByteArray data)
{
    qDebug() << "uart_recv_from_main_window" << data;

    /*子窗口存储日志*/
    QString str = data;
    bspFileCtrl.write_run_log(QtInfoMsg, bspTimerCtrl.get_current_timestamp(),"RX->"+str);

    /*接收原始数据回显*/
    str = ("<font color=blue>[" + bspTimerCtrl.get_current_timestamp() + "]:RX-><font color=black><br>" + str);
    ui->textEdit_rawOutput->append(str);

    if(TRUE == AtProtocol::at_protocol_analyse(&data, &atPrtcl.analyse_cmd, &atPrtcl.analyse_buff))
    {
        QByteArray idArray;
        sNodeListParam param;
        uint8_t cnt = 1;
        /*解析到有效数据，应用处理*/
        switch(atPrtcl.analyse_cmd)
        {
            /*AT测试*/
            case E_AT:
                /*解析输出显示*/
                str = "--------------------<br>";
                str += "AT测试OK";
                ui->textEdit_analyseOutput->append(str);
                break;

            /*获取版本*/
            case E_AT_GET_VERSION:
                /*解析输出显示*/
                str = "--------------------<br>";
                str += "获取版本OK:<br>";
                str += "ID: ";
                str += AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 1);
                str += "<br>";
                str += "software ver: ";
                str += AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 2);
                str += "<br>";
                str += "hardware ver: ";
                str += AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 3);
                ui->textEdit_analyseOutput->append(str);
                /*列表输出*/
                param.id = AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 1);
                param.rssi = "不修改";
                param.state = "不修改";
                param.type = "不修改";
                param.version.clear();
                param.version.append(AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 2));
                param.version.append(',');
                param.version.append(AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 3));
                networking_ctrl.update_node_param(&param);
                networking_ctrl.table_show(devicelist_item_model);
                emit table_item_sync_to_main_window_signal();
                break;

            /*设备重启*/
            case E_AT_RESET:
                /*解析输出显示*/
                str = "--------------------<br>";
                str += "设备重启OK:<br>";
                str += "ID: ";
                str += AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 1);
                ui->textEdit_analyseOutput->append(str);
                break;

            /*设备重置*/
            case E_AT_RESTORE:
                /*解析输出显示*/
                str = "--------------------<br>";
                str += "设备重置OK:<br>";
                str += "ID: ";
                str += AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 1);
                ui->textEdit_analyseOutput->append(str);
                break;

            /*扫描*/
            case E_AT_SCAN_NODE:
                /*解析输出显示*/
                str = "--------------------<br>";
                str += "设备扫描OK:<br>";
                str += "ID: ";
                str += AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 1);
                str += "<br>";
                str += "RSSI: ";
                str += AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 2);
                str += "<br>";
                str += "State: ";
                str += AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 3);
                ui->textEdit_analyseOutput->append(str);
                /*列表输出*/
                param.id = AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 1);
                param.rssi = AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 2);
                param.state = AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 3);
                param.type = "不修改";
                param.version = "不修改";
                networking_ctrl.update_node_param(&param);
                networking_ctrl.table_show(devicelist_item_model);
                emit table_item_sync_to_main_window_signal();
                break;

            /*添加节点*/
            case E_AT_ADD_NODE:
                /*解析输出显示*/
                str = "--------------------<br>";
                str += "添加节点OK:<br>";
                str += "ID: ";
                str += AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 1);
                ui->textEdit_analyseOutput->append(str);
                /*列表输出*/
                networking_ctrl.add_node(AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 1));
                networking_ctrl.table_show(devicelist_item_model);
                emit table_item_sync_to_main_window_signal();
                break;

            /*删除节点*/
            case E_AT_DELETE_NODE:
                str = "--------------------<br>";
                str += "删除节点OK:<br>";
                str += "ID: ";
                str += AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 1);
                ui->textEdit_analyseOutput->append(str);
                /*列表输出*/
                networking_ctrl.delete_node(AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 1));
                networking_ctrl.table_show(devicelist_item_model);
                emit table_item_sync_to_main_window_signal();
                break;

            /*替换节点列表*/
            case E_AT_SET_NODELIST:
                str = "--------------------<br>";
                str += "替换节点列表OK:";
                idArray = AtProtocol::get_ack_param(&atPrtcl.analyse_buff, cnt);
                networking_ctrl.clear_node_list();
                while(0 < idArray.length())
                {
                    str += "<br>";
                    str += "ID: ";
                    str += idArray;
                    cnt++;
                    idArray = AtProtocol::get_ack_param(&atPrtcl.analyse_buff, cnt);
                    networking_ctrl.add_node(AtProtocol::get_ack_param(&atPrtcl.analyse_buff, cnt));
                }
                ui->textEdit_analyseOutput->append(str);
                /*列表输出*/
                networking_ctrl.table_show(devicelist_item_model);
                emit table_item_sync_to_main_window_signal();
                break;

            /*获取节点列表*/
            case E_AT_GET_NODELIST:
                str = "--------------------<br>";
                str += "获取节点列表OK:";
                idArray = AtProtocol::get_ack_param(&atPrtcl.analyse_buff, cnt);
                while(0 < idArray.length())
                {
                    str += "<br>";
                    str += "ID: ";
                    str += idArray;
                    networking_ctrl.add_node(idArray);
                    cnt++;
                    idArray = AtProtocol::get_ack_param(&atPrtcl.analyse_buff, cnt);
                }
                ui->textEdit_analyseOutput->append(str);
                /*列表输出*/
                networking_ctrl.table_show(devicelist_item_model);
                emit table_item_sync_to_main_window_signal();
                break;

            /*清除节点状态*/
            case E_AT_CLEAR_NODE_STATE:
                str = "--------------------<br>";
                str += "清除节点状态OK:<br>";
                str += "ID: ";
                str += AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 1);
                ui->textEdit_analyseOutput->append(str);
                break;

            /*获取节点状态*/
            case E_AT_GET_NODE_STATE:
                str = "--------------------<br>";
                str += "获取节点状态OK:<br>";
                str += "ID: ";
                str += AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 1);
                str += "<br>";
                str += "Type: ";
                str += AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 2);
                str += "<br>";
                str += "Version: ";
                str += AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 3);
                str += "<br>";
                str += "RSSI: ";
                str += AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 4);
                str += "<br>";
                str += "STATE: ";
                str += AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 5);
                ui->textEdit_analyseOutput->append(str);
                /*列表输出*/
                param.id = AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 1);
                param.type = AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 2);
                param.version = AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 3);
                param.rssi = AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 4);
                param.state = AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 5);
                networking_ctrl.update_node_param(&param);
                networking_ctrl.table_show(devicelist_item_model);
                emit table_item_sync_to_main_window_signal();
                break;

            /*透传*/
            case E_AT_TRANSFER:
                str = "--------------------<br>";
                str += "数据透传OK:<br>";
                str += "ID: ";
                str += AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 1);
                ui->textEdit_analyseOutput->append(str);
                break;

            /*升级*/
            case E_AT_UPDATE:
                str = "--------------------<br>";
                str += "升级行OK:<br>";
                str += "ID: ";
                str += AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 1);
                str += "<br>";
                str += "ADDR: ";
                str += AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 2);
                str += "<br>";
                str += "STATE: ";
                str += AtProtocol::get_ack_param(&atPrtcl.analyse_buff, 3);
                ui->textEdit_analyseOutput->append(str);
                /*升级帧需要传递给升级状态机继续解析*/
                updateCtrl.updateRecvFlg = 1;
                updateCtrl.recvPack = atPrtcl.analyse_buff;
                break;
        }
    }
}

/*子窗口准备传递串口数据给子窗口*/
void SystemConfig::uart_send_to_main_window(QByteArray data)
{
    /*子窗口存储日志*/
    QString str = (data);
    bspFileCtrl.write_run_log(QtInfoMsg, bspTimerCtrl.get_current_timestamp(),"TX->"+str);
    /*发送原始数据回显*/
    str = ("<font color=green>[" + bspTimerCtrl.get_current_timestamp() + "]:TX-><font color=black><br>" + str);
    ui->textEdit_rawOutput->append(str);

    /*传递串口数据至主窗口*/
    emit uart_send_to_main_window_signal(data);
}

/*主窗口接收到子窗口的列表同步信息*/
void MainWindow::table_item_recv_from_widget(void)
{
    main_devicelist_item_model->removeRows(0, main_devicelist_item_model->rowCount ());
    for(uint8_t i = 0; i < sys_config_widget->devicelist_item_model->rowCount(); i++)
    {
        main_devicelist_item_model->setItem(i, 0, sys_config_widget->devicelist_item_model->item(i, 0)->clone());
        main_devicelist_item_model->setItem(i, 1, sys_config_widget->devicelist_item_model->item(i, 1)->clone());
        main_devicelist_item_model->setItem(i, 2, sys_config_widget->devicelist_item_model->item(i, 2)->clone());
        main_devicelist_item_model->setItem(i, 3, sys_config_widget->devicelist_item_model->item(i, 3)->clone());
        main_devicelist_item_model->setItem(i, 4, sys_config_widget->devicelist_item_model->item(i, 4)->clone());
    }
}

/*子窗口接收到主窗口的手动刷新指令*/
void SystemConfig::manual_refresh_list_from_main_window(void)
{
    /*step1:扫描*/
    atPrtcl.assemble_cmd = E_AT_SCAN_NODE;
    atPrtcl.assemble_buff.resize(0);
    atPrtcl.analyse_cmd = atPrtcl.assemble_cmd;
    atPrtcl.analyse_id.resize(0);
    if(TRUE == atPrtcl.at_protocol_assemble(atPrtcl.assemble_cmd, &atPrtcl.assemble_buff, &atPrtcl.assemble_buff))
    {
        uart_send_to_main_window(atPrtcl.assemble_buff);
    }
    /*等待500ms*/
    QTime _Timer = QTime::currentTime().addMSecs(500);
    while( QTime::currentTime() < _Timer )
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);


    /*step2:获取当前节点列表*/
    atPrtcl.assemble_cmd = E_AT_GET_NODELIST;
    atPrtcl.assemble_buff.resize(0);
    atPrtcl.analyse_cmd = atPrtcl.assemble_cmd;
    atPrtcl.analyse_id.resize(0);
    if(TRUE == atPrtcl.at_protocol_assemble(atPrtcl.assemble_cmd, &atPrtcl.assemble_buff, &atPrtcl.assemble_buff))
    {
        uart_send_to_main_window(atPrtcl.assemble_buff);
    }
    /*等待100ms*/
    _Timer = QTime::currentTime().addMSecs(100);
    while( QTime::currentTime() < _Timer )
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);


    /*step3:依次获取已添加设备的状态*/
    for(uint8_t i = 0; i < networking_ctrl.node_list_total_num; i++)
    {
        atPrtcl.assemble_cmd = E_AT_GET_NODE_STATE;
        atPrtcl.assemble_buff.resize(0);
        atPrtcl.analyse_cmd = atPrtcl.assemble_cmd;
        atPrtcl.analyse_id.resize(0);
        atPrtcl.assemble_buff.append(networking_ctrl.nodeListParam[i].id);
        atPrtcl.analyse_id.append(atPrtcl.assemble_buff);

        if(TRUE == atPrtcl.at_protocol_assemble(atPrtcl.assemble_cmd, &atPrtcl.assemble_buff, &atPrtcl.assemble_buff))
        {
            uart_send_to_main_window(atPrtcl.assemble_buff);
        }

        /*等待100ms*/
        _Timer = QTime::currentTime().addMSecs(100);
        while( QTime::currentTime() < _Timer )
            QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }

}

