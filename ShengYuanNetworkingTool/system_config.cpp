#include "system_config.h"
#include "ui_system_config.h"

#ifndef TRUE
#define TRUE (1)
#endif

#ifndef FALSE
#define FALSE (0)
#endif

SystemConfig::SystemConfig(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::SystemConfig)
{
    ui->setupUi(this);

    /*风格美化*/
    this->setStyleSheet("*{outline:0px;}QWidget#frmFlatUI{background:#FFFFFF;}");

    /*槽关联*/
    /*模式变动响应*/
    connect(ui->comboBox_config_mode, SIGNAL(currentIndexChanged(int)), this, SLOT(combox_changed_config_mode_change(int)));

    /*组网维护相关指令响应*/
    connect(ui->pushButton_scanDevice, SIGNAL(clicked()), this, SLOT(push_button_clicked_scan_device()));
    connect(ui->pushButton_checkDeviceList, SIGNAL(clicked()), this, SLOT(push_button_clicked_check_device_list()));
    connect(ui->pushButton_addDevice, SIGNAL(clicked()), this, SLOT(push_button_clicked_add_device()));
    connect(ui->pushButton_deleteDevice, SIGNAL(clicked()), this, SLOT(push_button_clicked_delete_device()));

    /*业务操作关指令响应*/
    connect(ui->pushButton_atTest, SIGNAL(clicked()), this, SLOT(push_button_clicked_at_test()));
    connect(ui->pushButton_getVersion, SIGNAL(clicked()), this, SLOT(push_button_clicked_get_version()));
    connect(ui->pushButton_reset, SIGNAL(clicked()), this, SLOT(push_button_clicked_reset()));
    connect(ui->pushButton_format, SIGNAL(clicked()), this, SLOT(push_button_clicked_format()));
    connect(ui->pushButton_getDeviceState, SIGNAL(clicked()), this, SLOT(push_button_clicked_get_device_state()));
    connect(ui->pushButton_clearDeviceState, SIGNAL(clicked()), this, SLOT(push_button_clicked_clear_device_state()));
    connect(ui->pushButton_transfer, SIGNAL(clicked()), this, SLOT(push_button_clicked_transfer()));

    /*隐藏窗口按钮响应*/
    connect(ui->pushButton_hide_output, SIGNAL(clicked()), this, SLOT(push_button_hide_output()));

    /*升级相关指令响应*/
    connect(ui->pushButton_selectUpdateFile, SIGNAL(clicked()), this, SLOT(push_button_clicked_select_update_file()));
    connect(ui->pushButton_update, SIGNAL(clicked()), this, SLOT(push_button_clicked_update()));
    connect(bspTimerCtrl.update_ack_timer, SIGNAL(timeout()), this, SLOT(ack_timeout_proccess()));
    connect(bspTimerCtrl.update_state_timer, SIGNAL(timeout()), this, SLOT(update_state_timeout_proccess()));

    /*系统配置初始化*/
    system_config_init();

    /*设置子窗口运行日志的存储地址*/
    bspFileCtrl.set_run_log_path("./system_config_log.txt");
    bspFileCtrl.write_run_log(QtInfoMsg, bspTimerCtrl.get_current_timestamp(),"start run tool");
}

SystemConfig::~SystemConfig()
{
    delete ui;
}
