#include "monitor_ctrl.h"
#include "mainwindow.h"
#include "system_config.h"
#include "ui_mainwindow.h"
#include "ui_system_config.h"


MonitorControl::MonitorControl() {}



/*主窗口初始化*/
void MainWindow::main_window_init(void)
{
    /*输出表格初始化*/
    main_devicelist_item_model = new QStandardItemModel();
    main_devicelist_item_model->setHorizontalHeaderItem(0, new QStandardItem(QObject::tr("设备ID")));
    main_devicelist_item_model->setHorizontalHeaderItem(1, new QStandardItem(QObject::tr("类型")));
    main_devicelist_item_model->setHorizontalHeaderItem(2, new QStandardItem(QObject::tr("版本")));
    main_devicelist_item_model->setHorizontalHeaderItem(3, new QStandardItem(QObject::tr("状态")));
    main_devicelist_item_model->setHorizontalHeaderItem(4, new QStandardItem(QObject::tr("RSSI")));

    ui->tableView_networking->setModel(main_devicelist_item_model);
    ui->tableView_networking->setColumnWidth(0,80);
    ui->tableView_networking->setColumnWidth(1,80);
    ui->tableView_networking->setColumnWidth(2,160);
    ui->tableView_networking->setColumnWidth(3,160);
    ui->tableView_networking->setColumnWidth(4,80);
    ui->tableView_networking->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView_networking->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableView_networking->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView_networking->setAlternatingRowColors(true);
    ui->tableView_networking->verticalHeader()->setVisible(false);
    ui->tableView_networking->horizontalHeader()->setStretchLastSection(true);
}


/*窗口大小发生改变*/
void MainWindow::resizeEvent(QResizeEvent *event)
{
    if(ui->tableView_networking->width() > 300)
    {
        ui->tableView_networking->setColumnWidth(0,ui->tableView_networking->width()*0.15);
        ui->tableView_networking->setColumnWidth(1,ui->tableView_networking->width()*0.10);
        ui->tableView_networking->setColumnWidth(2,ui->tableView_networking->width()*0.3);
        ui->tableView_networking->setColumnWidth(3,ui->tableView_networking->width()*0.3);
        ui->tableView_networking->setColumnWidth(4,ui->tableView_networking->width()*0.15);
        ui->tableView_networking->horizontalHeader()->setStretchLastSection(true);
    }
}


/*主窗口手动刷新列表*/
void MainWindow::push_button_clicked_manual_refresh(void)
{
    emit manual_refresh_list_signal();
}

/*主窗口自动刷新列表*/
void MainWindow::check_box_clicked_auto_refresh(void)
{
    if(true == ui->checkBox_auto_refresh->isChecked())
    {
        auto_refresh_time = ui->lineEdit_auto_refresh->text().toULong();
        auto_refresh_timeout_cnt = 0;
        qDebug() << auto_refresh_time;

        bspTimerCtrl.start_timer(bspTimerCtrl.auto_refresh_timer, 1000);
    }
    else
    {
        bspTimerCtrl.stop_timer(bspTimerCtrl.auto_refresh_timer);
    }
}

void MainWindow::auto_refresh_timeout(void)
{
    auto_refresh_timeout_cnt++;
    if(auto_refresh_timeout_cnt >= auto_refresh_time)
    {
        /*执行刷新操作*/
        emit ui->pushButton_manual_refresh->clicked();
    }
    bspTimerCtrl.start_timer(bspTimerCtrl.auto_refresh_timer, SERIAL_OPEN_CHECK_TIMEOUT);
}

/*系统配置子窗口初始化*/
void SystemConfig::system_config_init(void)
{
    /*模式选择初始化*/
    ui->comboBox_config_mode->addItem("本机操作");
    ui->comboBox_config_mode->addItem("点对点操作");
    ui->comboBox_config_mode->addItem("广播操作");
    ui->comboBox_config_mode->setCurrentIndex(0);
    monitor_ctrl.eConfigMode = E_CONFIG_LOACL_MODE;
    /*禁用ID配置*/
    ui->lineEdit_destId->setEnabled(false);
    ui->pushButton_addDevice->setEnabled(false);
    ui->pushButton_deleteDevice->setEnabled(false);
    /*输出表格初始化*/
    devicelist_item_model = new QStandardItemModel();
    devicelist_item_model->setHorizontalHeaderItem(0, new QStandardItem(QObject::tr("设备ID")));
    devicelist_item_model->setHorizontalHeaderItem(1, new QStandardItem(QObject::tr("类型")));
    devicelist_item_model->setHorizontalHeaderItem(2, new QStandardItem(QObject::tr("版本")));
    devicelist_item_model->setHorizontalHeaderItem(3, new QStandardItem(QObject::tr("状态")));
    devicelist_item_model->setHorizontalHeaderItem(4, new QStandardItem(QObject::tr("RSSI")));
    ui->tableView_deviceList->setModel(devicelist_item_model);
    ui->tableView_deviceList->setColumnWidth(0,45);
    ui->tableView_deviceList->setColumnWidth(1,30);
    ui->tableView_deviceList->setColumnWidth(2,90);
    ui->tableView_deviceList->setColumnWidth(3,90);
    ui->tableView_deviceList->setColumnWidth(4,45);
    ui->tableView_deviceList->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView_deviceList->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableView_deviceList->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView_deviceList->setAlternatingRowColors(true);
    ui->tableView_deviceList->verticalHeader()->setVisible(false);
    ui->tableView_deviceList->horizontalHeader()->setStretchLastSection(true);

    ui->comboBox_updateTarget->addItem("MCU升级");
    ui->comboBox_updateTarget->addItem("RF芯片升级");
    ui->comboBox_updateTarget->setCurrentIndex(0);

    /*载入升级文件地址*/
    if(true == bspFileCtrl.is_setting_exist(E_SETTING_UPDATE_PATH))
    {
        updateCtrl.updateFilePath = bspFileCtrl.get_setting(E_SETTING_UPDATE_PATH).toString();
        ui->lineEdit_updateFilePath->setText(bspFileCtrl.get_setting(E_SETTING_UPDATE_PATH).toString());
    }
}

/*窗口大小发生改变*/
void SystemConfig::resizeEvent(QResizeEvent *event)
{
    if(ui->tableView_deviceList->width() > 300)
    {
        ui->tableView_deviceList->setColumnWidth(0,ui->tableView_deviceList->width()*0.15);
        ui->tableView_deviceList->setColumnWidth(1,ui->tableView_deviceList->width()*0.10);
        ui->tableView_deviceList->setColumnWidth(2,ui->tableView_deviceList->width()*0.3);
        ui->tableView_deviceList->setColumnWidth(3,ui->tableView_deviceList->width()*0.3);
        ui->tableView_deviceList->setColumnWidth(4,ui->tableView_deviceList->width()*0.15);
    }
    else
    {
        ui->tableView_deviceList->setColumnWidth(0,45);
        ui->tableView_deviceList->setColumnWidth(1,30);
        ui->tableView_deviceList->setColumnWidth(2,90);
        ui->tableView_deviceList->setColumnWidth(3,90);
        ui->tableView_deviceList->setColumnWidth(4,45);
    }
    ui->tableView_deviceList->horizontalHeader()->setStretchLastSection(true);
}

/*隐藏窗口响应*/
void SystemConfig::push_button_hide_output(void)
{
    if(">" == ui->pushButton_hide_output->text())
    {
        /*隐藏输出*/
        ui->pushButton_hide_output->setText("<");
        ui->groupBox_2->hide();
        ui->groupBox_3->hide();
    }
    else
    {
        /*显示输出*/
        ui->pushButton_hide_output->setText(">");
        ui->groupBox_2->show();
        ui->groupBox_3->show();
    }

    /*等待10ms*/
    QTime _Timer = QTime::currentTime().addMSecs(10);
    while( QTime::currentTime() < _Timer )
        QCoreApplication::processEvents(QEventLoop::AllEvents, 10);

    /*重新调整表格列宽*/
    if(ui->tableView_deviceList->width() > 300)
    {
        ui->tableView_deviceList->setColumnWidth(0,ui->tableView_deviceList->width()*0.15);
        ui->tableView_deviceList->setColumnWidth(1,ui->tableView_deviceList->width()*0.10);
        ui->tableView_deviceList->setColumnWidth(2,ui->tableView_deviceList->width()*0.3);
        ui->tableView_deviceList->setColumnWidth(3,ui->tableView_deviceList->width()*0.3);
        ui->tableView_deviceList->setColumnWidth(4,ui->tableView_deviceList->width()*0.15);
    }
    else
    {
        ui->tableView_deviceList->setColumnWidth(0,45);
        ui->tableView_deviceList->setColumnWidth(1,30);
        ui->tableView_deviceList->setColumnWidth(2,90);
        ui->tableView_deviceList->setColumnWidth(3,90);
        ui->tableView_deviceList->setColumnWidth(4,45);
    }
    ui->tableView_deviceList->horizontalHeader()->setStretchLastSection(true);

}

/*操作模式变化*/
void SystemConfig::combox_changed_config_mode_change(int index)
{
    if(0 == index)
    {
        /*本地操作模式*/
        monitor_ctrl.eConfigMode = E_CONFIG_LOACL_MODE;
        /*禁用远程操作相关配置*/
        ui->lineEdit_destId->setEnabled(false);
        ui->pushButton_addDevice->setEnabled(false);
        ui->pushButton_deleteDevice->setEnabled(false);
    }
    else if(1 == index)
    {
        /*远程操作模式*/
        monitor_ctrl.eConfigMode = E_CONFIG_REMOTE_MODE;
        ui->lineEdit_destId->setEnabled(true);
        ui->pushButton_addDevice->setEnabled(true);
        ui->pushButton_deleteDevice->setEnabled(true);
    }
    else
    {
        /*广播操作模式*/
        monitor_ctrl.eConfigMode = E_CONFIG_BROADCAST_MODE;
        ui->lineEdit_destId->setEnabled(false);
        ui->pushButton_addDevice->setEnabled(false);
        ui->pushButton_deleteDevice->setEnabled(false);
    }
}

/*AT测试*/
void SystemConfig::push_button_clicked_at_test(void)
{
    atPrtcl.assemble_cmd = E_AT;
    atPrtcl.assemble_buff.resize(0);
    atPrtcl.analyse_cmd = atPrtcl.assemble_cmd;
    atPrtcl.analyse_id.resize(0);
    if(TRUE == atPrtcl.at_protocol_assemble(atPrtcl.assemble_cmd, &atPrtcl.assemble_buff, &atPrtcl.assemble_buff))
    {
        uart_send_to_main_window(atPrtcl.assemble_buff);
    }
}

/*获取版本*/
void SystemConfig::push_button_clicked_get_version(void)
{
    atPrtcl.assemble_cmd = E_AT_GET_VERSION;
    atPrtcl.assemble_buff.resize(0);
    atPrtcl.analyse_cmd = atPrtcl.assemble_cmd;
    atPrtcl.analyse_id.resize(0);
    if(E_CONFIG_LOACL_MODE == monitor_ctrl.eConfigMode)
    {
        atPrtcl.assemble_buff.append("FFFFFFFF");
    }
    else if(E_CONFIG_BROADCAST_MODE == monitor_ctrl.eConfigMode)
    {
        atPrtcl.assemble_buff.append("AAAAAAAA");
    }
    else
    {
        atPrtcl.assemble_buff.append(ui->lineEdit_destId->text().toUtf8());
    }
    atPrtcl.analyse_id.append(atPrtcl.assemble_buff);

    if(TRUE == atPrtcl.at_protocol_assemble(atPrtcl.assemble_cmd, &atPrtcl.assemble_buff, &atPrtcl.assemble_buff))
    {
        uart_send_to_main_window(atPrtcl.assemble_buff);
    }
}

/*重启*/
void SystemConfig::push_button_clicked_reset(void)
{
    atPrtcl.assemble_cmd = E_AT_RESET;
    atPrtcl.assemble_buff.resize(0);
    atPrtcl.analyse_cmd = atPrtcl.assemble_cmd;
    atPrtcl.analyse_id.resize(0);
    if(E_CONFIG_LOACL_MODE == monitor_ctrl.eConfigMode)
    {
        atPrtcl.assemble_buff.append("FFFFFFFF");
    }
    else if(E_CONFIG_BROADCAST_MODE == monitor_ctrl.eConfigMode)
    {
        atPrtcl.assemble_buff.append("AAAAAAAA");
    }
    else
    {
        atPrtcl.assemble_buff.append(ui->lineEdit_destId->text().toUtf8());
    }
    atPrtcl.analyse_id.append(atPrtcl.assemble_buff);

    if(TRUE == atPrtcl.at_protocol_assemble(atPrtcl.assemble_cmd, &atPrtcl.assemble_buff, &atPrtcl.assemble_buff))
    {
        uart_send_to_main_window(atPrtcl.assemble_buff);
    }
}

/*格式化*/
void SystemConfig::push_button_clicked_format(void)
{
    atPrtcl.assemble_cmd = E_AT_RESTORE;
    atPrtcl.assemble_buff.resize(0);
    atPrtcl.analyse_cmd = atPrtcl.assemble_cmd;
    atPrtcl.analyse_id.resize(0);
    if(E_CONFIG_LOACL_MODE == monitor_ctrl.eConfigMode)
    {
        atPrtcl.assemble_buff.append("FFFFFFFF");
    }
    else if(E_CONFIG_BROADCAST_MODE == monitor_ctrl.eConfigMode)
    {
        atPrtcl.assemble_buff.append("AAAAAAAA");
    }
    else
    {
        atPrtcl.assemble_buff.append(ui->lineEdit_destId->text().toUtf8());
    }
    atPrtcl.analyse_id.append(atPrtcl.assemble_buff);

    if(TRUE == atPrtcl.at_protocol_assemble(atPrtcl.assemble_cmd, &atPrtcl.assemble_buff, &atPrtcl.assemble_buff))
    {
        uart_send_to_main_window(atPrtcl.assemble_buff);
    }
}

/*获取设备状态*/
void SystemConfig::push_button_clicked_get_device_state(void)
{
    atPrtcl.assemble_cmd = E_AT_GET_NODE_STATE;
    atPrtcl.assemble_buff.resize(0);
    atPrtcl.analyse_cmd = atPrtcl.assemble_cmd;
    atPrtcl.analyse_id.resize(0);
    if(E_CONFIG_LOACL_MODE == monitor_ctrl.eConfigMode)
    {
        atPrtcl.assemble_buff.append("FFFFFFFF");
    }
    else if(E_CONFIG_BROADCAST_MODE == monitor_ctrl.eConfigMode)
    {
        atPrtcl.assemble_buff.append("AAAAAAAA");
    }
    else
    {
        atPrtcl.assemble_buff.append(ui->lineEdit_destId->text().toUtf8());
    }
    atPrtcl.analyse_id.append(atPrtcl.assemble_buff);

    if(TRUE == atPrtcl.at_protocol_assemble(atPrtcl.assemble_cmd, &atPrtcl.assemble_buff, &atPrtcl.assemble_buff))
    {
        uart_send_to_main_window(atPrtcl.assemble_buff);
    }
}

/*清除设备状态*/
void SystemConfig::push_button_clicked_clear_device_state(void)
{
    atPrtcl.assemble_cmd = E_AT_CLEAR_NODE_STATE;
    atPrtcl.assemble_buff.resize(0);
    atPrtcl.analyse_cmd = atPrtcl.assemble_cmd;
    atPrtcl.analyse_id.resize(0);
    if(E_CONFIG_LOACL_MODE == monitor_ctrl.eConfigMode)
    {
        atPrtcl.assemble_buff.append("FFFFFFFF");
    }
    else if(E_CONFIG_BROADCAST_MODE == monitor_ctrl.eConfigMode)
    {
        atPrtcl.assemble_buff.append("AAAAAAAA");
    }
    else
    {
        atPrtcl.assemble_buff.append(ui->lineEdit_destId->text().toUtf8());
    }
    atPrtcl.analyse_id.append(atPrtcl.assemble_buff);

    if(TRUE == atPrtcl.at_protocol_assemble(atPrtcl.assemble_cmd, &atPrtcl.assemble_buff, &atPrtcl.assemble_buff))
    {
        uart_send_to_main_window(atPrtcl.assemble_buff);
    }
}

/*数据透传*/
void SystemConfig::push_button_clicked_transfer(void)
{
    atPrtcl.assemble_cmd = E_AT_TRANSFER;
    atPrtcl.assemble_buff.resize(0);
    atPrtcl.analyse_cmd = atPrtcl.assemble_cmd;
    atPrtcl.analyse_id.resize(0);
    if(E_CONFIG_LOACL_MODE == monitor_ctrl.eConfigMode)
    {
        atPrtcl.assemble_buff.append("FFFFFFFF");
    }
    else if(E_CONFIG_BROADCAST_MODE == monitor_ctrl.eConfigMode)
    {
        atPrtcl.assemble_buff.append("AAAAAAAA");
    }
    else
    {
        atPrtcl.assemble_buff.append(ui->lineEdit_destId->text().toUtf8());
    }
    atPrtcl.analyse_id.append(atPrtcl.assemble_buff);
    atPrtcl.assemble_buff.append(",");
    atPrtcl.assemble_buff.append(ui->textEdit_transfer->toPlainText().toUtf8());

    if(TRUE == atPrtcl.at_protocol_assemble(atPrtcl.assemble_cmd, &atPrtcl.assemble_buff, &atPrtcl.assemble_buff))
    {
        uart_send_to_main_window(atPrtcl.assemble_buff);
    }
}
