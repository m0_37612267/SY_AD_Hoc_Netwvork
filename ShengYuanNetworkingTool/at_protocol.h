#ifndef AT_PROTOCOL_H
#define AT_PROTOCOL_H

#include <QString>

/**
 * 主机模组本地/下行AT指令:
 * AT 测试AT启动
 * AT+GETVERSION=[ID] 查询软硬件版本
 * AT+RESET=[ID] 重启
 * AT+RESTORE=[ID] 恢复出厂设置
 * AT+SCANNODE 扫描附近的设备
 * AT+ADDNODE=[ID],[ID]... 添加设备
 * AT+DELETENODE=[ID],[ID]... 删除设备
 * AT+NODELIST[?] 查看/替换当前添加的设备列表
 * AT+NODESTATE[?]=[ID]+ 获取/清除设备状态
 * AT+UPDATE=[ID],[HEX] 升级，使用hex文件格式
 * AT+TRANSFER=[ID],[STR] 下行透传
 * AT+UPDATE=[ID],[TYPE],[HEX]
 *
 * [ID]参数说明
 *      必须是完整的ID序号，
 *      非组网指令可缺失，缺失时默认操作所有远程/本地设备
 *      本机设备ID可填写全0xFF
 *      所有远程设备ID(即广播)可填写全0xAA
 *      部分指令可填写多个ID
 *
 * [TYPE]参数说明
 *      指定升级对象
 *      0：MCU升级
 *      1：射频模组升级
 *
 * [HEX]参数说明
 *      需为完整的一行hex数据，去除：
 *
 * 从机模组本地/上行AT指令:
 * AT 测试AT启动
 * AT+GETVERSION 查询软硬件版本
 * AT+RESET 重启
 * AT+RESTORE 恢复出厂设置
 * AT+UPDATE=[ID],[HEX] 本地升级，使用hex文件格式
 * AT+TRANSFR=[ID],[STR] 上行透传
 *
 *
 * 示例：
 * TX-> AT
 * RX-> OK
 *
 * TX-> AT+GETVERSION=12345678
 * RX-> ID: 12345678
 * RX-> software ver: V01.00.00
 * RX-> hardware ver: V01.00.00
 * RX-> OK
 *
 * TX-> AT+RESET=AAAAAAAA
 * RX-> ID: 12345678
 * RX-> OK
 * RX-> ID: 22345678
 * RX-> OK
 *
 * TX-> AT+RESTORE=12345678
 * RX-> ID: 12345678
 * RX-> OK
 *
 * TX-> AT+SCANNODE
 * RX-> ID: 12345678
 * RX-> RSSI: -70,-77
 * RX-> State: 已添加
 * RX-> OK
 * RX-> ID: 22345678
 * RX-> RSSI: -70,-77
 * RX-> State: 未添加
 * RX-> OK
 *
 * TX-> AT+ADDNODE=12345678
 * RX-> ID: 12345678
 * RX-> OK
 *
 * TX-> AT+DELETENODE=12345678
 * RX-> ID: 12345678
 * RX-> OK
 *
 * TX-> AT+NODELIST?
 * RX-> ID: 12345678
 * RX-> OK
 *
 * TX-> AT+NODSTATE?=12345678
 * RX-> ID: 12345678
 * RX-> Type: 从机
 * RX-> Version: V01.00.00,V01.00.00
 * RX-> RSSI: -70,-77
 * RX-> State: 正常
 * RX-> OK
 *
 * TX-> AT+NODSTATE=12345678
 * RX-> ID: 12345678
 * RX-> OK
 *
 * TX-> AT+TRANSFER=12345678,blablabla
 * RX-> ID: 12345678
 * RX-> OK
 *
 * TX-> AT+UPDATE=12345678,0,020000040800F2
 * RX-> ID: 12345678
 * RX-> Addr: 0000
 * RX-> State: 0
 * RX-> OK
 * */

typedef enum
{
    E_AT = 0,
    E_AT_GET_VERSION = 1,
    E_AT_RESET = 2,
    E_AT_RESTORE = 3,
    E_AT_SCAN_NODE = 4,
    E_AT_ADD_NODE = 5,
    E_AT_DELETE_NODE = 6,
    E_AT_SET_NODELIST = 7,
    E_AT_GET_NODELIST = 8,
    E_AT_CLEAR_NODE_STATE = 9,
    E_AT_GET_NODE_STATE = 10,
    E_AT_TRANSFER = 11,
    E_AT_UPDATE = 12,
}E_AT_CMD;

class AtProtocol
{
public:
    E_AT_CMD analyse_cmd;
    QByteArray analyse_id;
    QByteArray analyse_buff;
    E_AT_CMD assemble_cmd;
    QByteArray assemble_buff;
    static uint8_t at_protocol_analyse(QByteArray* input_data, E_AT_CMD* cmd, QByteArray* analyse_buff);
    static uint8_t at_protocol_assemble(E_AT_CMD assemble_cmd, QByteArray* assemble_buff, QByteArray* output_buff);
    static QByteArray get_ack_param(QByteArray* input_data, uint8_t param_index);
};

#endif // AT_PROTOCOL_H


