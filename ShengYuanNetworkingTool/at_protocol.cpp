#include "mainwindow.h"
#include "at_protocol.h"
#include "qdebug.h"


/*协议解析*/
uint8_t AtProtocol::at_protocol_analyse(QByteArray* input_data, E_AT_CMD* cmd, QByteArray* analyse_buff)
{
    QByteArray byteArray;
    int index = 0;
    int end_index = 0;
    uint8_t ret = FALSE;

    byteArray.resize(input_data->length());
    byteArray = *input_data;

    qDebug() << byteArray;

#ifdef AT_RECEIVER
    /*找到帧头*/
    while(-1 != (index = byteArray.indexOf("AT", index)))
    {
        // qDebug() << index;

        /*找到帧尾*/
        if(-1 == (end_index = byteArray.indexOf('\r', index)))
        {
            end_index = byteArray.length();
        }
        end_index--;
        // qDebug() << end_index;

        if(1 == (end_index - index))
        {
            /*测试AT启动*/
            *cmd = E_AT;
            analyse_buff.resize(0);
            ret = TRUE;
            qDebug() << "E_AT";
        }
        else if((12 <= (end_index - index))
            &&(0 == memcmp(&byteArray[index + 2], "+GETVERSION", 11)))
        {
            /*查询软硬件版本*/
            *cmd = E_AT_GET_VERSION;
            if(12 < (end_index - index))
            {
                analyse_buff.resize(end_index - index - 13);
                memcpy(&analyse_buff[0], &byteArray[index + 14], analyse_buff.length());
            }
            else
            {
                analyse_buff.resize(0);
            }
            ret = TRUE;
            qDebug() << "E_AT_GET_VERSION" << analyse_buff;
        }

        index++;
        if(index >= byteArray.length())
        {
            break;
        }
    }

#else

    /*找到帧尾*/
    while(-1 != (end_index = byteArray.indexOf("OK", end_index)))
    {
        end_index++;
        qDebug() << index;

        if(1 == (end_index - index))
        {
            analyse_buff->resize(0);
            ret = TRUE;
            qDebug() << "OK";
        }
        else
        {
            analyse_buff->resize(end_index - index - 3);
            memcpy(&(*analyse_buff)[0], &byteArray[index], analyse_buff->length());
            ret = TRUE;
            qDebug() << *analyse_buff;
        }

        end_index++;
        index = end_index;
        if(end_index >= byteArray.length())
        {
            break;
        }
    }

#endif

    return ret;
}

/*协议组帧*/
uint8_t AtProtocol::at_protocol_assemble(E_AT_CMD assemble_cmd, QByteArray* assemble_buff, QByteArray* output_buff)
{
    QByteArray byteArray;
    uint8_t ret = FALSE;

    byteArray.resize(0);

    byteArray.append("AT");
    if(E_AT == assemble_cmd)
    {
        ret = TRUE;
    }
    else if(E_AT_GET_VERSION == assemble_cmd)
    {
        byteArray.append("+GETVERSION");
        if(0 < assemble_buff->length())
        {
            byteArray.append("=");
            byteArray.append(*assemble_buff);
        }
        ret = TRUE;
    }
    else if(E_AT_RESET == assemble_cmd)
    {
        byteArray.append("+RESET");
        if(0 < assemble_buff->length())
        {
            byteArray.append("=");
            byteArray.append(*assemble_buff);
        }
        ret = TRUE;
    }
    else if(E_AT_RESTORE == assemble_cmd)
    {
        byteArray.append("+RESTORE");
        if(0 < assemble_buff->length())
        {
            byteArray.append("=");
            byteArray.append(*assemble_buff);
        }
        ret = TRUE;
    }
    else if(E_AT_SCAN_NODE == assemble_cmd)
    {
        byteArray.append("+SCANNODE");
        ret = TRUE;
    }
    else if(E_AT_ADD_NODE == assemble_cmd)
    {
        byteArray.append("+ADDNODE");
        if(0 < assemble_buff->length())
        {
            byteArray.append("=");
            byteArray.append(*assemble_buff);
        }
        ret = TRUE;
    }
    else if(E_AT_DELETE_NODE == assemble_cmd)
    {
        byteArray.append("+DELETENODE");
        if(0 < assemble_buff->length())
        {
            byteArray.append("=");
            byteArray.append(*assemble_buff);
        }
        ret = TRUE;
    }
    else if(E_AT_SET_NODELIST == assemble_cmd)
    {
        byteArray.append("+NODELIST");
        if(0 < assemble_buff->length())
        {
            byteArray.append("=");
            byteArray.append(*assemble_buff);
        }
        ret = TRUE;
    }
    else if(E_AT_GET_NODELIST == assemble_cmd)
    {
        byteArray.append("+NODELIST?");
        ret = TRUE;
    }
    else if(E_AT_CLEAR_NODE_STATE == assemble_cmd)
    {
        byteArray.append("+NODESTATE");
        if(0 < assemble_buff->length())
        {
            byteArray.append("=");
            byteArray.append(*assemble_buff);
        }
        ret = TRUE;
    }
    else if(E_AT_GET_NODE_STATE == assemble_cmd)
    {
        byteArray.append("+NODESTATE?");
        if(0 < assemble_buff->length())
        {
            byteArray.append("=");
            byteArray.append(*assemble_buff);
        }
        ret = TRUE;
    }
    else if(E_AT_TRANSFER == assemble_cmd)
    {
        byteArray.append("+TRANSFER");
        if(0 < assemble_buff->length())
        {
            byteArray.append("=");
            byteArray.append(*assemble_buff);
        }
        ret = TRUE;
    }
    else if(E_AT_UPDATE == assemble_cmd)
    {
        byteArray.append("+UPDATE");
        if(0 < assemble_buff->length())
        {
            byteArray.append("=");
            byteArray.append(*assemble_buff);
        }
        ret = TRUE;
    }

    if(TRUE == ret)
    {
        byteArray.append('\r');
        byteArray.append('\n');
        *output_buff = byteArray;
    }

    return ret;
}

/*提取应答帧中的参数*/
QByteArray AtProtocol::get_ack_param(QByteArray* input_data, uint8_t param_index)
{
    int start_offset = 0;
    int end_offset;
    QByteArray ret;

    ret.resize(0);
    while(0 < param_index)
    {
        start_offset = input_data->indexOf(": ", start_offset);
        if(-1 == start_offset)
        {
            return ret;
        }
        start_offset += 2;
        end_offset = input_data->indexOf('\r', start_offset);
        if(-1 == end_offset)
        {
            end_offset = input_data->length();
        }

        param_index--;
        if(0 == param_index)
        {
            ret.append(input_data->mid(start_offset, end_offset - start_offset));
            qDebug() << "get_ack_param" << *input_data << ret;
            return ret;
        }
    }

    return ret;
}
