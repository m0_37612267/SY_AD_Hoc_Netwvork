/********************************************************************************
** Form generated from reading UI file 'system_config.ui'
**
** Created by: Qt User Interface Compiler version 6.5.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SYSTEM_CONFIG_H
#define UI_SYSTEM_CONFIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SystemConfig
{
public:
    QHBoxLayout *horizontalLayout_2;
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_4;
    QGroupBox *groupBox_5;
    QHBoxLayout *horizontalLayout_3;
    QComboBox *comboBox_config_mode;
    QLabel *label;
    QLineEdit *lineEdit_destId;
    QGroupBox *groupBox_6;
    QGridLayout *gridLayout_2;
    QTextEdit *textEdit_transfer;
    QPushButton *pushButton_clearDeviceState;
    QPushButton *pushButton_transfer;
    QPushButton *pushButton_atTest;
    QPushButton *pushButton_getDeviceState;
    QPushButton *pushButton_format;
    QPushButton *pushButton_reset;
    QPushButton *pushButton_getVersion;
    QGroupBox *groupBox_7;
    QGridLayout *gridLayout;
    QPushButton *pushButton_scanDevice;
    QPushButton *pushButton_checkDeviceList;
    QPushButton *pushButton_addDevice;
    QPushButton *pushButton_deleteDevice;
    QGroupBox *groupBox_8;
    QGridLayout *gridLayout_3;
    QLineEdit *lineEdit_updateFilePath;
    QPushButton *pushButton_selectUpdateFile;
    QComboBox *comboBox_updateTarget;
    QPushButton *pushButton_update;
    QProgressBar *progressBar_update;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_5;
    QTableView *tableView_deviceList;
    QPushButton *pushButton_hide_output;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_6;
    QTextEdit *textEdit_rawOutput;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_7;
    QTextEdit *textEdit_analyseOutput;

    void setupUi(QWidget *SystemConfig)
    {
        if (SystemConfig->objectName().isEmpty())
            SystemConfig->setObjectName("SystemConfig");
        SystemConfig->resize(1188, 521);
        horizontalLayout_2 = new QHBoxLayout(SystemConfig);
        horizontalLayout_2->setObjectName("horizontalLayout_2");
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(1);
        horizontalLayout->setObjectName("horizontalLayout");
        groupBox_4 = new QGroupBox(SystemConfig);
        groupBox_4->setObjectName("groupBox_4");
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBox_4->sizePolicy().hasHeightForWidth());
        groupBox_4->setSizePolicy(sizePolicy);
        groupBox_4->setMinimumSize(QSize(224, 446));
        gridLayout_4 = new QGridLayout(groupBox_4);
        gridLayout_4->setObjectName("gridLayout_4");
        groupBox_5 = new QGroupBox(groupBox_4);
        groupBox_5->setObjectName("groupBox_5");
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox_5->sizePolicy().hasHeightForWidth());
        groupBox_5->setSizePolicy(sizePolicy1);
        horizontalLayout_3 = new QHBoxLayout(groupBox_5);
        horizontalLayout_3->setObjectName("horizontalLayout_3");
        comboBox_config_mode = new QComboBox(groupBox_5);
        comboBox_config_mode->setObjectName("comboBox_config_mode");

        horizontalLayout_3->addWidget(comboBox_config_mode);

        label = new QLabel(groupBox_5);
        label->setObjectName("label");

        horizontalLayout_3->addWidget(label);

        lineEdit_destId = new QLineEdit(groupBox_5);
        lineEdit_destId->setObjectName("lineEdit_destId");

        horizontalLayout_3->addWidget(lineEdit_destId);


        gridLayout_4->addWidget(groupBox_5, 0, 0, 1, 1);

        groupBox_6 = new QGroupBox(groupBox_4);
        groupBox_6->setObjectName("groupBox_6");
        gridLayout_2 = new QGridLayout(groupBox_6);
        gridLayout_2->setObjectName("gridLayout_2");
        textEdit_transfer = new QTextEdit(groupBox_6);
        textEdit_transfer->setObjectName("textEdit_transfer");
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(textEdit_transfer->sizePolicy().hasHeightForWidth());
        textEdit_transfer->setSizePolicy(sizePolicy2);

        gridLayout_2->addWidget(textEdit_transfer, 3, 0, 1, 2);

        pushButton_clearDeviceState = new QPushButton(groupBox_6);
        pushButton_clearDeviceState->setObjectName("pushButton_clearDeviceState");

        gridLayout_2->addWidget(pushButton_clearDeviceState, 2, 1, 1, 1);

        pushButton_transfer = new QPushButton(groupBox_6);
        pushButton_transfer->setObjectName("pushButton_transfer");

        gridLayout_2->addWidget(pushButton_transfer, 4, 0, 1, 2);

        pushButton_atTest = new QPushButton(groupBox_6);
        pushButton_atTest->setObjectName("pushButton_atTest");

        gridLayout_2->addWidget(pushButton_atTest, 0, 0, 1, 1);

        pushButton_getDeviceState = new QPushButton(groupBox_6);
        pushButton_getDeviceState->setObjectName("pushButton_getDeviceState");

        gridLayout_2->addWidget(pushButton_getDeviceState, 2, 0, 1, 1);

        pushButton_format = new QPushButton(groupBox_6);
        pushButton_format->setObjectName("pushButton_format");

        gridLayout_2->addWidget(pushButton_format, 1, 1, 1, 1);

        pushButton_reset = new QPushButton(groupBox_6);
        pushButton_reset->setObjectName("pushButton_reset");

        gridLayout_2->addWidget(pushButton_reset, 1, 0, 1, 1);

        pushButton_getVersion = new QPushButton(groupBox_6);
        pushButton_getVersion->setObjectName("pushButton_getVersion");

        gridLayout_2->addWidget(pushButton_getVersion, 0, 1, 1, 1);


        gridLayout_4->addWidget(groupBox_6, 2, 0, 1, 1);

        groupBox_7 = new QGroupBox(groupBox_4);
        groupBox_7->setObjectName("groupBox_7");
        sizePolicy1.setHeightForWidth(groupBox_7->sizePolicy().hasHeightForWidth());
        groupBox_7->setSizePolicy(sizePolicy1);
        gridLayout = new QGridLayout(groupBox_7);
        gridLayout->setObjectName("gridLayout");
        pushButton_scanDevice = new QPushButton(groupBox_7);
        pushButton_scanDevice->setObjectName("pushButton_scanDevice");

        gridLayout->addWidget(pushButton_scanDevice, 0, 0, 1, 1);

        pushButton_checkDeviceList = new QPushButton(groupBox_7);
        pushButton_checkDeviceList->setObjectName("pushButton_checkDeviceList");

        gridLayout->addWidget(pushButton_checkDeviceList, 0, 1, 1, 1);

        pushButton_addDevice = new QPushButton(groupBox_7);
        pushButton_addDevice->setObjectName("pushButton_addDevice");

        gridLayout->addWidget(pushButton_addDevice, 1, 0, 1, 1);

        pushButton_deleteDevice = new QPushButton(groupBox_7);
        pushButton_deleteDevice->setObjectName("pushButton_deleteDevice");

        gridLayout->addWidget(pushButton_deleteDevice, 1, 1, 1, 1);


        gridLayout_4->addWidget(groupBox_7, 1, 0, 1, 1);

        groupBox_8 = new QGroupBox(groupBox_4);
        groupBox_8->setObjectName("groupBox_8");
        sizePolicy1.setHeightForWidth(groupBox_8->sizePolicy().hasHeightForWidth());
        groupBox_8->setSizePolicy(sizePolicy1);
        gridLayout_3 = new QGridLayout(groupBox_8);
        gridLayout_3->setObjectName("gridLayout_3");
        lineEdit_updateFilePath = new QLineEdit(groupBox_8);
        lineEdit_updateFilePath->setObjectName("lineEdit_updateFilePath");
        lineEdit_updateFilePath->setEnabled(false);

        gridLayout_3->addWidget(lineEdit_updateFilePath, 0, 2, 1, 2);

        pushButton_selectUpdateFile = new QPushButton(groupBox_8);
        pushButton_selectUpdateFile->setObjectName("pushButton_selectUpdateFile");

        gridLayout_3->addWidget(pushButton_selectUpdateFile, 0, 1, 1, 1);

        comboBox_updateTarget = new QComboBox(groupBox_8);
        comboBox_updateTarget->setObjectName("comboBox_updateTarget");

        gridLayout_3->addWidget(comboBox_updateTarget, 0, 0, 1, 1);

        pushButton_update = new QPushButton(groupBox_8);
        pushButton_update->setObjectName("pushButton_update");

        gridLayout_3->addWidget(pushButton_update, 1, 0, 1, 1);

        progressBar_update = new QProgressBar(groupBox_8);
        progressBar_update->setObjectName("progressBar_update");
        progressBar_update->setValue(0);

        gridLayout_3->addWidget(progressBar_update, 1, 1, 1, 3);


        gridLayout_4->addWidget(groupBox_8, 3, 0, 1, 1);


        horizontalLayout->addWidget(groupBox_4);

        groupBox = new QGroupBox(SystemConfig);
        groupBox->setObjectName("groupBox");
        groupBox->setMinimumSize(QSize(225, 446));
        gridLayout_5 = new QGridLayout(groupBox);
        gridLayout_5->setObjectName("gridLayout_5");
        gridLayout_5->setHorizontalSpacing(5);
        gridLayout_5->setContentsMargins(5, -1, 3, -1);
        tableView_deviceList = new QTableView(groupBox);
        tableView_deviceList->setObjectName("tableView_deviceList");
        tableView_deviceList->setMinimumSize(QSize(221, 421));

        gridLayout_5->addWidget(tableView_deviceList, 0, 0, 1, 1);


        horizontalLayout->addWidget(groupBox);

        pushButton_hide_output = new QPushButton(SystemConfig);
        pushButton_hide_output->setObjectName("pushButton_hide_output");
        QSizePolicy sizePolicy3(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(100);
        sizePolicy3.setHeightForWidth(pushButton_hide_output->sizePolicy().hasHeightForWidth());
        pushButton_hide_output->setSizePolicy(sizePolicy3);
        pushButton_hide_output->setMinimumSize(QSize(15, 50));
        pushButton_hide_output->setMaximumSize(QSize(15, 50));

        horizontalLayout->addWidget(pushButton_hide_output);

        groupBox_2 = new QGroupBox(SystemConfig);
        groupBox_2->setObjectName("groupBox_2");
        groupBox_2->setMinimumSize(QSize(225, 446));
        gridLayout_6 = new QGridLayout(groupBox_2);
        gridLayout_6->setObjectName("gridLayout_6");
        gridLayout_6->setHorizontalSpacing(5);
        gridLayout_6->setContentsMargins(3, -1, 5, -1);
        textEdit_rawOutput = new QTextEdit(groupBox_2);
        textEdit_rawOutput->setObjectName("textEdit_rawOutput");
        textEdit_rawOutput->setMinimumSize(QSize(221, 421));

        gridLayout_6->addWidget(textEdit_rawOutput, 0, 1, 1, 1);


        horizontalLayout->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(SystemConfig);
        groupBox_3->setObjectName("groupBox_3");
        groupBox_3->setMinimumSize(QSize(226, 446));
        gridLayout_7 = new QGridLayout(groupBox_3);
        gridLayout_7->setObjectName("gridLayout_7");
        gridLayout_7->setContentsMargins(5, -1, 5, -1);
        textEdit_analyseOutput = new QTextEdit(groupBox_3);
        textEdit_analyseOutput->setObjectName("textEdit_analyseOutput");
        textEdit_analyseOutput->setMinimumSize(QSize(221, 421));

        gridLayout_7->addWidget(textEdit_analyseOutput, 0, 0, 1, 1);


        horizontalLayout->addWidget(groupBox_3);


        horizontalLayout_2->addLayout(horizontalLayout);


        retranslateUi(SystemConfig);

        QMetaObject::connectSlotsByName(SystemConfig);
    } // setupUi

    void retranslateUi(QWidget *SystemConfig)
    {
        SystemConfig->setWindowTitle(QCoreApplication::translate("SystemConfig", "Form", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("SystemConfig", "\346\214\207\344\273\244\350\276\223\345\205\245", nullptr));
        groupBox_5->setTitle(QCoreApplication::translate("SystemConfig", "\347\233\256\346\240\207ID\351\205\215\347\275\256", nullptr));
        label->setText(QCoreApplication::translate("SystemConfig", "\347\233\256\346\240\207ID", nullptr));
        groupBox_6->setTitle(QCoreApplication::translate("SystemConfig", "\344\270\232\345\212\241\346\223\215\344\275\234", nullptr));
        pushButton_clearDeviceState->setText(QCoreApplication::translate("SystemConfig", "\346\270\205\351\231\244\350\256\276\345\244\207\347\212\266\346\200\201", nullptr));
        pushButton_transfer->setText(QCoreApplication::translate("SystemConfig", "\351\200\217\344\274\240", nullptr));
        pushButton_atTest->setText(QCoreApplication::translate("SystemConfig", "AT\346\265\213\350\257\225", nullptr));
        pushButton_getDeviceState->setText(QCoreApplication::translate("SystemConfig", "\350\216\267\345\217\226\350\256\276\345\244\207\347\212\266\346\200\201", nullptr));
        pushButton_format->setText(QCoreApplication::translate("SystemConfig", "\350\256\276\345\244\207\351\207\215\347\275\256", nullptr));
        pushButton_reset->setText(QCoreApplication::translate("SystemConfig", "\351\207\215\345\220\257", nullptr));
        pushButton_getVersion->setText(QCoreApplication::translate("SystemConfig", "\346\237\245\350\257\242\347\211\210\346\234\254", nullptr));
        groupBox_7->setTitle(QCoreApplication::translate("SystemConfig", "\347\273\204\347\275\221\347\273\264\346\212\244\346\223\215\344\275\234", nullptr));
        pushButton_scanDevice->setText(QCoreApplication::translate("SystemConfig", "\346\211\253\346\217\217\350\256\276\345\244\207", nullptr));
        pushButton_checkDeviceList->setText(QCoreApplication::translate("SystemConfig", "\346\237\245\347\234\213\350\256\276\345\244\207\345\210\227\350\241\250", nullptr));
        pushButton_addDevice->setText(QCoreApplication::translate("SystemConfig", "\346\267\273\345\212\240\350\256\276\345\244\207", nullptr));
        pushButton_deleteDevice->setText(QCoreApplication::translate("SystemConfig", "\345\210\240\351\231\244\350\256\276\345\244\207", nullptr));
        groupBox_8->setTitle(QCoreApplication::translate("SystemConfig", "\345\215\207\347\272\247", nullptr));
        pushButton_selectUpdateFile->setText(QCoreApplication::translate("SystemConfig", "\351\200\211\346\213\251\346\226\207\344\273\266", nullptr));
        pushButton_update->setText(QCoreApplication::translate("SystemConfig", "\345\274\200\345\247\213\345\215\207\347\272\247", nullptr));
        groupBox->setTitle(QCoreApplication::translate("SystemConfig", "\350\256\276\345\244\207\345\210\227\350\241\250", nullptr));
        pushButton_hide_output->setText(QCoreApplication::translate("SystemConfig", ">", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("SystemConfig", "\345\216\237\345\247\213\350\276\223\345\207\272", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("SystemConfig", "\350\247\243\346\236\220\350\276\223\345\207\272", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SystemConfig: public Ui_SystemConfig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SYSTEM_CONFIG_H
