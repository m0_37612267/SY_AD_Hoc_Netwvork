#ifndef BSP_FILE_H
#define BSP_FILE_H

#include <QFileDialog>

typedef enum
{
    E_SETTING_UART_BAUDRATE,
    E_SETTING_MODE,
    E_SETTING_UPDATE_PATH,
    E_SETTING_DEST_SN,
    E_SETTING_ROUTE_SN,
    E_SETTING_POINT_TABLE,
    E_SETTING_ID_LIST,
    E_SETTING_SAVE_PATH,
}E_SETTING_TYPE;

class BspFileControl
{
public:
    void save_setting(E_SETTING_TYPE type, QVariant data);
    QVariant get_setting(E_SETTING_TYPE type);
    bool is_setting_exist(E_SETTING_TYPE type);
    QString select_hex_file(QWidget* obj, QString dir);
    static QString select_uart_log(QWidget* obj, QString dir);
    static QString select_save_uart_log(QWidget* obj, QString dir);
    static QString select_point_table_config(QWidget* obj, QString dir);
    static QString select_id_list_config(QWidget* obj, QString dir);
    void set_run_log_path(QString path);
    static bool is_path_exist(uint8_t mkpathEn, QString dirPath);
    void write_run_log(QtMsgType type, QString time, QString log);
private:
    QString logPath;
};

#endif // BSP_FILE_H
