#ifndef UPDATE_CTRL_H
#define UPDATE_CTRL_H

#include <QString>

typedef enum
{
    E_UPGRADE_IDLE,
    E_UPGRADE_START,
    E_UPGRADE_READY,
    E_SEND_PACK,
    E_WAIT_ACK,
    E_RESEND_PACK,
    E_FINISH,
}E_UPGRADE_STATE;

class UpdateCtrl
{
public:
    UpdateCtrl();
    QString updateFilePath;
    QByteArray sendPack;
    QByteArray recvPack;
    uint8_t updateRecvFlg;
    uint32_t fullPackSize;
    uint32_t currentSize;
    uint8_t updatePercent;
    E_UPGRADE_STATE eState;
    uint8_t Update_Print_Flg;
    uint8_t Loop_Update_Flg;
    QByteArray latchAddr;
};

#endif // UPDATE_CTRL_H
