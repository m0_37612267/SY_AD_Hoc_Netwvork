#ifndef SYSTEM_CONFIG_H
#define SYSTEM_CONFIG_H

#include <QWidget>
#include <QStandardItemModel>
#include "bsp_file.h"
#include "bsp_timer.h"
#include "monitor_ctrl.h"
#include "at_protocol.h"
#include "networking_control.h"
#include "update_ctrl.h"


#ifndef TRUE
#define TRUE (1)
#endif

#ifndef FALSE
#define FALSE (0)
#endif

namespace Ui {
class SystemConfig;
}

class SystemConfig : public QWidget
{
    Q_OBJECT

public:
    explicit SystemConfig(QWidget *parent = nullptr);
    ~SystemConfig();

    /*设备列表输出表格*/
    QStandardItemModel* devicelist_item_model;

private:
    Ui::SystemConfig *ui;
    /*链路驱动相关对象*/
    BspFileControl bspFileCtrl;
    BspTimerControl bspTimerCtrl;
    /*日常运行应用逻辑对象*/
    MonitorControl monitor_ctrl;
    /*协议处理对象*/
    AtProtocol atPrtcl;
    /*组网列表对象*/
    NetworkingControl networking_ctrl;
    /*升级控制对象*/
    UpdateCtrl updateCtrl;

    /*系统配置初始化*/
    void system_config_init(void);
    /*升级*/
    void upgrade_state_machine(void);


protected:
    void resizeEvent(QResizeEvent *event);

private slots:
    /*模式变动响应*/
    void combox_changed_config_mode_change(int index);
    /*组网维护相关指令响应*/
    void push_button_clicked_scan_device(void);
    void push_button_clicked_check_device_list(void);
    void push_button_clicked_add_device(void);
    void push_button_clicked_delete_device(void);
    /*业务操作关指令响应*/
    void push_button_clicked_at_test(void);
    void push_button_clicked_get_version(void);
    void push_button_clicked_reset(void);
    void push_button_clicked_format(void);
    void push_button_clicked_get_device_state(void);
    void push_button_clicked_clear_device_state(void);
    void push_button_clicked_transfer(void);
    /*隐藏窗口按钮响应*/
    void push_button_hide_output(void);
    /*升级操作指令响应*/
    void push_button_clicked_select_update_file(void);
    void push_button_clicked_update(void);
    /*主窗口数据交互*/
    void uart_recv_from_main_window(QByteArray data);
    void uart_send_to_main_window(QByteArray data);
    void manual_refresh_list_from_main_window(void);
    /*升级状态超时*/
    void ack_timeout_proccess(void);
    void update_state_timeout_proccess(void);

signals:
    void uart_send_to_main_window_signal(QByteArray data);
    void table_item_sync_to_main_window_signal(void);

};

#endif // SYSTEM_CONFIG_H
