/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.5.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionfenxigongju;
    QAction *action1;
    QAction *action_system_config;
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout_2;
    QComboBox *comboBox_uartBaudRate;
    QComboBox *comboBox_uartPort;
    QPushButton *pushButton_uartRefresh;
    QPushButton *pushButton_uartOpen;
    QSpacerItem *verticalSpacer;
    QCheckBox *checkBox_function_test;
    QPushButton *pushButton_manual_refresh;
    QCheckBox *checkBox_auto_refresh;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_auto_refresh;
    QLineEdit *lineEdit_auto_refresh;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_networking_config;
    QTableView *tableView_networking;
    QMenuBar *menubar;
    QMenu *menu;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName("MainWindow");
        MainWindow->resize(790, 500);
        actionfenxigongju = new QAction(MainWindow);
        actionfenxigongju->setObjectName("actionfenxigongju");
        action1 = new QAction(MainWindow);
        action1->setObjectName("action1");
        action_system_config = new QAction(MainWindow);
        action_system_config->setObjectName("action_system_config");
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName("centralwidget");
        horizontalLayout_3 = new QHBoxLayout(centralwidget);
        horizontalLayout_3->setObjectName("horizontalLayout_3");
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName("verticalLayout_3");
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName("verticalLayout_2");
        comboBox_uartBaudRate = new QComboBox(centralwidget);
        comboBox_uartBaudRate->setObjectName("comboBox_uartBaudRate");
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(comboBox_uartBaudRate->sizePolicy().hasHeightForWidth());
        comboBox_uartBaudRate->setSizePolicy(sizePolicy);
        comboBox_uartBaudRate->setMinimumSize(QSize(129, 40));

        verticalLayout_2->addWidget(comboBox_uartBaudRate);

        comboBox_uartPort = new QComboBox(centralwidget);
        comboBox_uartPort->setObjectName("comboBox_uartPort");
        sizePolicy.setHeightForWidth(comboBox_uartPort->sizePolicy().hasHeightForWidth());
        comboBox_uartPort->setSizePolicy(sizePolicy);
        comboBox_uartPort->setMinimumSize(QSize(129, 40));

        verticalLayout_2->addWidget(comboBox_uartPort);

        pushButton_uartRefresh = new QPushButton(centralwidget);
        pushButton_uartRefresh->setObjectName("pushButton_uartRefresh");
        sizePolicy.setHeightForWidth(pushButton_uartRefresh->sizePolicy().hasHeightForWidth());
        pushButton_uartRefresh->setSizePolicy(sizePolicy);
        pushButton_uartRefresh->setMinimumSize(QSize(129, 40));

        verticalLayout_2->addWidget(pushButton_uartRefresh);

        pushButton_uartOpen = new QPushButton(centralwidget);
        pushButton_uartOpen->setObjectName("pushButton_uartOpen");
        sizePolicy.setHeightForWidth(pushButton_uartOpen->sizePolicy().hasHeightForWidth());
        pushButton_uartOpen->setSizePolicy(sizePolicy);
        pushButton_uartOpen->setMinimumSize(QSize(129, 40));

        verticalLayout_2->addWidget(pushButton_uartOpen);


        verticalLayout_3->addLayout(verticalLayout_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);

        checkBox_function_test = new QCheckBox(centralwidget);
        checkBox_function_test->setObjectName("checkBox_function_test");

        verticalLayout_3->addWidget(checkBox_function_test);

        pushButton_manual_refresh = new QPushButton(centralwidget);
        pushButton_manual_refresh->setObjectName("pushButton_manual_refresh");
        sizePolicy.setHeightForWidth(pushButton_manual_refresh->sizePolicy().hasHeightForWidth());
        pushButton_manual_refresh->setSizePolicy(sizePolicy);
        pushButton_manual_refresh->setMinimumSize(QSize(129, 40));

        verticalLayout_3->addWidget(pushButton_manual_refresh);

        checkBox_auto_refresh = new QCheckBox(centralwidget);
        checkBox_auto_refresh->setObjectName("checkBox_auto_refresh");
        sizePolicy.setHeightForWidth(checkBox_auto_refresh->sizePolicy().hasHeightForWidth());
        checkBox_auto_refresh->setSizePolicy(sizePolicy);
        checkBox_auto_refresh->setCheckable(true);

        verticalLayout_3->addWidget(checkBox_auto_refresh);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName("horizontalLayout_2");
        label_auto_refresh = new QLabel(centralwidget);
        label_auto_refresh->setObjectName("label_auto_refresh");
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_auto_refresh->sizePolicy().hasHeightForWidth());
        label_auto_refresh->setSizePolicy(sizePolicy1);

        horizontalLayout_2->addWidget(label_auto_refresh);

        lineEdit_auto_refresh = new QLineEdit(centralwidget);
        lineEdit_auto_refresh->setObjectName("lineEdit_auto_refresh");
        lineEdit_auto_refresh->setEnabled(true);
        sizePolicy.setHeightForWidth(lineEdit_auto_refresh->sizePolicy().hasHeightForWidth());
        lineEdit_auto_refresh->setSizePolicy(sizePolicy);
        lineEdit_auto_refresh->setMaximumSize(QSize(50, 16777215));

        horizontalLayout_2->addWidget(lineEdit_auto_refresh);


        verticalLayout_3->addLayout(horizontalLayout_2);


        horizontalLayout_3->addLayout(verticalLayout_3);

        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName("groupBox_2");
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy2);
        groupBox_2->setMinimumSize(QSize(631, 301));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setSpacing(0);
        gridLayout_2->setObjectName("gridLayout_2");
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName("verticalLayout");
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName("horizontalLayout");
        label = new QLabel(groupBox_2);
        label->setObjectName("label");

        horizontalLayout->addWidget(label);

        horizontalSpacer = new QSpacerItem(37, 17, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_networking_config = new QPushButton(groupBox_2);
        pushButton_networking_config->setObjectName("pushButton_networking_config");
        sizePolicy1.setHeightForWidth(pushButton_networking_config->sizePolicy().hasHeightForWidth());
        pushButton_networking_config->setSizePolicy(sizePolicy1);
        pushButton_networking_config->setMinimumSize(QSize(0, 0));

        horizontalLayout->addWidget(pushButton_networking_config);


        verticalLayout->addLayout(horizontalLayout);

        tableView_networking = new QTableView(groupBox_2);
        tableView_networking->setObjectName("tableView_networking");

        verticalLayout->addWidget(tableView_networking);


        gridLayout_2->addLayout(verticalLayout, 0, 0, 1, 1);


        horizontalLayout_3->addWidget(groupBox_2);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName("menubar");
        menubar->setGeometry(QRect(0, 0, 790, 20));
        menu = new QMenu(menubar);
        menu->setObjectName("menu");
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName("statusbar");
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menu->menuAction());
        menu->addAction(action_system_config);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        actionfenxigongju->setText(QCoreApplication::translate("MainWindow", "\345\210\206\346\236\220\345\267\245\345\205\267", nullptr));
        action1->setText(QCoreApplication::translate("MainWindow", "1", nullptr));
        action_system_config->setText(QCoreApplication::translate("MainWindow", "\347\263\273\347\273\237\351\205\215\347\275\256", nullptr));
        pushButton_uartRefresh->setText(QCoreApplication::translate("MainWindow", "\345\210\267\346\226\260\344\270\262\345\217\243", nullptr));
        pushButton_uartOpen->setText(QCoreApplication::translate("MainWindow", "\346\211\223\345\274\200\344\270\262\345\217\243", nullptr));
        checkBox_function_test->setText(QCoreApplication::translate("MainWindow", "\344\270\212\344\275\215\346\234\272\345\212\237\350\203\275\350\207\252\346\265\213", nullptr));
        pushButton_manual_refresh->setText(QCoreApplication::translate("MainWindow", "\346\211\213\345\212\250\346\225\260\346\215\256\345\210\267\346\226\260", nullptr));
        checkBox_auto_refresh->setText(QCoreApplication::translate("MainWindow", "\350\207\252\345\212\250\345\210\267\346\226\260\344\275\277\350\203\275", nullptr));
        label_auto_refresh->setText(QCoreApplication::translate("MainWindow", "\350\207\252\345\212\250\345\210\267\346\226\260\351\227\264\351\232\224(s)", nullptr));
        lineEdit_auto_refresh->setText(QCoreApplication::translate("MainWindow", "10", nullptr));
        groupBox_2->setTitle(QString());
        label->setText(QCoreApplication::translate("MainWindow", "\346\225\264\347\253\231\350\256\276\345\244\207\344\277\241\346\201\257\346\246\202\350\247\210", nullptr));
        pushButton_networking_config->setText(QCoreApplication::translate("MainWindow", "\351\205\215\347\275\256\346\223\215\344\275\234", nullptr));
        menu->setTitle(QCoreApplication::translate("MainWindow", "\350\217\234\345\215\225", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
