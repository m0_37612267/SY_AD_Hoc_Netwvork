#ifndef UART_TRSPRT_H
#define UART_TRSPRT_H

#include <QString>

typedef struct
{
    QString serial_recv_time;
    QByteArray serial_recv_buf;
}SerialTrsprtControl;

#endif // UART_TRSPRT_H
