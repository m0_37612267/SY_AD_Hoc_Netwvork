#include "bsp_file.h"
#include <QSettings>
#include <QTextStream>

QSettings cfg("config.ini",QSettings::IniFormat);

/*保存配置*/
void BspFileControl::save_setting(E_SETTING_TYPE type, QVariant data)
{
    switch(type)
    {
    case E_SETTING_UART_BAUDRATE:
        cfg.setValue("uart baud",data);
        break;
    case E_SETTING_MODE:
        cfg.setValue("mode",data);
        break;
    case E_SETTING_UPDATE_PATH:
        cfg.setValue("update path",data);
        break;
    case E_SETTING_DEST_SN:
        cfg.setValue("dest sn",data);
        break;
    case E_SETTING_ROUTE_SN:
        cfg.setValue("route sn",data);
        break;
    case E_SETTING_POINT_TABLE:
        cfg.setValue("point table",data);
        break;
    case E_SETTING_SAVE_PATH:
        cfg.setValue("save path",data);
        break;
    default:
        return;
    }
}

/*读取配置*/
QVariant BspFileControl::get_setting(E_SETTING_TYPE type)
{
    switch(type)
    {
    case E_SETTING_UART_BAUDRATE:
        return cfg.value("uart baud");
    case E_SETTING_MODE:
        return cfg.value("mode");
    case E_SETTING_UPDATE_PATH:
        return cfg.value("update path");
    case E_SETTING_DEST_SN:
        return cfg.value("dest sn");
    case E_SETTING_ROUTE_SN:
        return cfg.value("route sn");
    case E_SETTING_POINT_TABLE:
        return cfg.value("point table");
    case E_SETTING_SAVE_PATH:
        return cfg.value("save path");
    default:
        return -1;
    }
}

/*确认配置是否有保存过*/
bool BspFileControl::is_setting_exist(E_SETTING_TYPE type)
{
    switch(type)
    {
    case E_SETTING_UART_BAUDRATE:
        return cfg.contains("uart baud");
    case E_SETTING_MODE:
        return cfg.contains("mode");
    case E_SETTING_UPDATE_PATH:
        return cfg.contains("update path");
    case E_SETTING_DEST_SN:
        return cfg.contains("dest sn");
    case E_SETTING_ROUTE_SN:
        return cfg.contains("route sn");
    case E_SETTING_POINT_TABLE:
        return cfg.contains("point table");
    case E_SETTING_SAVE_PATH:
        return cfg.contains("save path");
    default:
        return false;
    }
}

/*选择无线/串口升级文件*/
QString BspFileControl::select_hex_file(QWidget* obj, QString dir)
{
    return QFileDialog::getOpenFileName(obj,QStringLiteral("文件对话框"),dir,QStringLiteral("HEX文件(*hex)"));
}

/*选择保存/读取的串口日志*/
QString BspFileControl::select_uart_log(QWidget* obj, QString dir)
{
    return QFileDialog::getOpenFileName(obj,QStringLiteral("文件对话框"),dir,QStringLiteral("文件(*txt;*log)"));
}

/*选择保存/读取的串口日志*/
QString BspFileControl::select_save_uart_log(QWidget* obj, QString dir)
{
    return QFileDialog::getSaveFileName(obj,QStringLiteral("文件对话框"),dir,QStringLiteral("文件(*txt;*log)"));
}

/*选择保存/读取的点表配置*/
QString BspFileControl::select_point_table_config(QWidget* obj, QString dir)
{
    return QFileDialog::getOpenFileName(obj,QStringLiteral("文件对话框"),dir,QStringLiteral("文件(*ini)"));
}

/*选择保存/读取的点表配置*/
QString BspFileControl::select_id_list_config(QWidget* obj, QString dir)
{
    return QFileDialog::getOpenFileName(obj,QStringLiteral("文件对话框"),dir,QStringLiteral("文件(*ini)"));
}

/*配置运行日志路径*/
void BspFileControl::set_run_log_path(QString path)
{
    logPath = path;
}

/*判断日志路径是否有效*/
bool BspFileControl::is_path_exist(uint8_t mkpathEn, QString dirPath)
{
    QDir dir(dirPath);
    if (dir.exists())
    {
        return true;
    }

    if(1 == mkpathEn)
    {
        return dir.mkpath(dirPath);
    }
    else
    {
        dir.mkpath(dirPath);
        return false;
    }
}

/*记录保存运行日志*/
void BspFileControl::write_run_log(QtMsgType type, QString time, QString log)
{
    QString logInfo;
    switch((uint8_t)type)
    {
    case QtDebugMsg:
        logInfo = QString("%1 [Debug] %2\n").arg(time, log);
        break;
    case QtWarningMsg:
        logInfo = QString("%1 [Warning] %2\n").arg(time, log);
        break;
    case QtCriticalMsg:
        logInfo = QString("%1 [Critical] %2\n").arg(time, log);
        break;
    case QtFatalMsg:
        logInfo = QString("%1 [Fatal] %2\n").arg(time, log);
        break;
    case QtInfoMsg:
        logInfo = QString("%1 [Info] %2\n").arg(time, log);
        break;
    default:
        return;
    }

    QFile outFile(logPath);
    QFileInfo fileInfo(outFile);
    if (!is_path_exist(1, fileInfo.absoluteDir().absolutePath()))
    {
        return;
    }

    if (!outFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text))
    {
        return;
    }

    QTextStream ts(&outFile);
    ts << logInfo;
    outFile.close();
}
